/**
 * Developed By     : Sree Bijoy Kumar Mohanta
 * Crated Date      : 12 July 2018
 * Purpose          : Starting Screen Entry Point of This Module
 * Project Name     : masterapp
 */

import { StackNavigator, DrawerNavigator } from "react-navigation";
import { AppRegistry } from "react-native"
import Splash from "../src/scenes/Splash";
import Login from "../src/scenes/Login";
import Detail from "../src/scenes/Detail";
import UserProfile from "../src/scenes/UserProfile";
import ChangePassword from "../src/scenes/ChangePassword"
import FilterList from "../src/scenes/FilteredList";
import Search from "../src/scenes/Search"
import Tab from "../src/navigation/navigations"
import * as ScreenNames from "../src/navigation/screenames";
import * as Color from "../src/styles/Color";
import MyLocation from "./scenes/MyLocation";

const appNavigator = StackNavigator({
  [ScreenNames.SPLASH]: {
    screen: Splash
  },
  [ScreenNames.LOGIN]: {
    screen: Login
  },
  [ScreenNames.DETAIL]: {
    screen: Tab,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  [ScreenNames.MY_LOCATION]: {
    screen: MyLocation,
    navigationOptions: {
      headerTitle: "My Location",
      headerStyle: {
        backgroundColor: Color.HEADER_COLOR,
        elevation: null,
        shadowOpacity: 0,
        marginTop: 20
      },
      headerTitleStyle: {
        color: "#ffffff",
        fontWeight: "500",
        fontSize: 18,
        textAlign: "center",
        flexGrow: 0.80
      },
      headerTintColor: "#ffffff"
    }
  },
  [ScreenNames.USER_PROFILE]: {
    screen: UserProfile,
    navigationOptions: {
      headerTitle: "Me",
      headerStyle: {
        backgroundColor: Color.HEADER_COLOR,
        elevation: null,
        shadowOpacity: 0,
        marginTop: 20
      },
      headerTitleStyle: {
        color: "#ffffff",
        fontWeight: "500",
        fontSize: 18,
        textAlign: "center",
        flexGrow: 0.80
      },
      headerTintColor: "#ffffff"
    }
  },
  [ScreenNames.CHANGE_PASSWORD]: {
    screen: ChangePassword,
    navigationOptions: {
      headerTitle: "Change Password",
      headerStyle: {
        backgroundColor: Color.HEADER_COLOR,
        elevation: null,
        shadowOpacity: 0,
        marginTop: 20
      },
      headerTitleStyle: {
        color: "#ffffff",
        fontWeight: "500",
        fontSize: 18,
        textAlign: "center",
        flexGrow: 0.80
      },
      headerTintColor: "#ffffff"
    }
  },
  [ScreenNames.FILTERED_LIST]: {
    screen: FilterList,
    navigationOptions: {
      headerTitle: "Filtered List",
      headerStyle: {
        backgroundColor: Color.HEADER_COLOR,
        elevation: null,
        shadowOpacity: 0,
        marginTop: 20
      },
      headerTitleStyle: {
        color: "#ffffff",
        fontWeight: "500",
        fontSize: 18,
        textAlign: "center",
        flexGrow: 0.80
      },
      headerTintColor: "#ffffff"
    }
  },
  [ScreenNames.SEARCHED]: {
    screen: Search,
    navigationOptions: {
      headerTitle: "Search",
      headerStyle: {
        backgroundColor: Color.HEADER_COLOR,
        elevation: null,
        shadowOpacity: 0,
        marginTop: 20
      },
      headerTitleStyle: {
        color: "#ffffff",
        fontWeight: "500",
        fontSize: 18,
        textAlign: "center",
        flexGrow: 0.80
      },
      headerTintColor: "#ffffff"
    }
  }
});
export const Drawer = DrawerNavigator({
  Stack: { screen: appNavigator },
  Simple1: { screen: Search },
  Simple2: { screen: Search },
  Simple3: { screen: Search }
})
AppRegistry.registerComponent('Drawer', () => Drawer);
export default Drawer;
