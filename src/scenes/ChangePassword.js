/**
 * Created By       : Sree Bijoy Kumar Mohanata
 * Date             : 24 July 2018
 * Purpose          : User Profile and Logout
 */
import AppContainer from "../styles/Container";
import AppStatusBar from "../components/AppStatusBar";
import React from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
  StatusBar,
  AsyncStorage
} from "react-native";
import Dimensions from "Dimensions";
import * as AppsColor from "../styles/Color";
var { width, height } = Dimensions.get("window");
export default class ChangePassword extends React.Component {
  constructor() {
    super();

    this.state = {
      nameHint: "",
      pinHint: "",
      emailHint: "",
      phoneHint: "",
      genderHint: "",
      professionHint: "",
      orgHintTxt: "",
      passwordHint: "",
      confirmPasswordHint: "",
      SecretAnswerHint: "",
      name: "",
      email: "",
      gender: "",
      profession: "",
      org: "",
      phone: "",
      pin: "",
      password: "",
      hasFocus0: false,
      hasFocus1: false,
      hasFocus2: false,
      hasFocus3: false,
      hasFocus4: false,
      hasFocus5: false,
      hasFocus6: false,
      fbAccessToken: "",
      fbUserID: "",
      errMsg: "",
      searchedAdresses: [],
      registrationStatus: "",
      confirmPassword: "",
      SecretQuestion: "",
      isLoader: false,
      answer: "",
      changeView: false,
    }


  }


  ValidatePhone(phone) {
    if (((phone.split("016")).length === 2 || (phone.split("017")).length === 2 ||
      (phone.split("018")).length === 2 || (phone.split("015")).length === 2 ||
      (phone.split("019")).length === 2) && phone.length === 11
    ) {
      return true;
    }
    else return false;
  }

  ValidatePin(pin) {
    if (pin.length <= 7) {
      return true;
    }
    else {
      return false;
    }
  }

  ValidateFields() {
    let flag = true;

    if (this.state.phone === undefined || this.state.phone.length !== 11) {
      flag = false;
    }

    if (this.state.pin === undefined || this.state.pin !== "") {
      flag = false;
    }
    return flag;
  }

  ValidateFieldsConfirmChange() {
    let flag = true;

    if (this.state.SecretAnswer === undefined || this.state.SecretAnswer.length === "") {
      flag = false;
    }

    if (this.state.password === undefined || this.state.password === "") {
      flag = false;
    }
    if (this.state.confirmPassword === undefined || this.state.confirmPassword === "" && this.ValidatePassword(this.state.confirmPassword)) {
      flag = false;
    }
    return flag;
  }

  _onBlur(hasFocus) {
    if (hasFocus.id === 0) {
      this.setState({ hasFocus0: false });
      this.getQuestion();
    } else if (hasFocus.id === 1) {
      this.setState({ hasFocus1: false });
    } else if (hasFocus.id === 2) {
      this.setState({ hasFocus2: false });
    } else if (hasFocus.id === 3) {
      this.setState({ hasFocus3: false });
    } else if (hasFocus.id === 4) {
      this.setState({ hasFocus4: false });
    } else if (hasFocus.id === 5) {
      this.setState({ hasFocus5: false });
    } else if (hasFocus.id === 6) {
      this.setState({ hasFocus6: false });
    }
  }

  _onFocus(hasFocus) {
    if (hasFocus.id === 0) {
      this.setState({ hasFocus0: true });
    } else if (hasFocus.id === 1) {
      this.setState({ hasFocus1: true });
    } else if (hasFocus.id === 2) {
      this.setState({ hasFocus2: true });
    } else if (hasFocus.id === 3) {
      this.setState({ hasFocus3: true });
    } else if (hasFocus.id === 4) {
      this.setState({ hasFocus4: true });
    } else if (hasFocus.id === 5) {
      this.setState({ hasFocus5: true });
    } else if (hasFocus.id === 6) {
      this.setState({ hasFocus6: true });
    }
  }

  _getULColor(hasFocus) {
    return hasFocus === true ? "#0aae7e" : "#dbe2e0";
  }

  toggleHint = text => {
    let pinHintTxt = "Your PIN is";
    let phoneHintTxt = "You Phone Number is";
    let passwordHintTxt = "Your Password";
    let confirmPasswordHintTxt = "Password Matched";
    let SecretAnswerHintTxt = this.state.SecretQuestion;

    if (text.id === 0) {
      if (text.text.length > 0 && this.ValidatePhone(text.text)) {
        this.setState({
          phoneHint: phoneHintTxt,
          phone: text.text
        });
      } else {
        this.setState({ phoneHint: "Enter a valid phone number", phone: "" });
      }
    } else if (text.id === 1) {
      if (text.text.length > 0) {
        this.setState({
          pinHint: pinHintTxt,
          SecretAnswer: text.text
        });
      } else {
        this.setState({ pinHint: "" });
      }
    } else if (text.id === 2) {
      if (text.text.length > 0) {
        this.setState({
          passwordHint: passwordHintTxt,
          password: text.text
        });
      } else {
        this.setState({ passwordHint: "" });
      }
    } else if (text.id === 3) {
      if (text.text.length > 0 && this.ValidatePassword(text.text)) {
        this.setState({
          confirmPasswordHint: confirmPasswordHintTxt,
          confirmPassword: text.text
        });
      } else {
        this.setState({ confirmPasswordHint: "Password Does Not Match", confirmPassword: "" });
      }
    } else if (text.id === 4) {
      if (text.text.length > 0) {
        this.setState({
          SecretAnswerHint: SecretAnswerHintTxt,
          SecretAnswer: text.text
        });
      } else {
        this.setState({ SecretAnswerHint: "" });
      }
    }
  };

  getQuestion() {
    fetch(
      "http://flippoll.hackio.xyz:8080/api/UserRegistration/GetUserSecretData",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          UserName: this.state.phone,
        })
      }
    )
      .then(response => response.json())
      .then(responseJson => {

        if (responseJson.ResponseStatus === "NotFetched") {
          this.setState({ registrationStatus: "*Please provide correct infromation" });
          return;
        }
        else if (responseJson.ResponseStatus === "Success") {
          let user = responseJson.User;
          let question = user.SecretQuestion;
          this.setState({ SecretQuestion: question })
          return responseJson.ResponseStatus;
        }

      })
      .catch(error => {
        console.error(error);
      });

  }

  confirmChange() {

    if (this.ValidateFieldsConfirmChange() === true) {
      const { navigation } = this.props;
      fetch(
        "http://flippoll.hackio.xyz:8080/api/UserRegistration/ChangePassword",
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            UserName: this.state.phone,
            SecretQuestion: this.state.SecretQuestion,
            SecretAnswer: this.state.SecretAnswer,
            Password: this.state.password,
          })
        }
      )
        .then(response => response.json())
        .then(responseJson => {

          if (responseJson.ResponseStatus === "Fail") {
            //alert("Failed");
            this.setState({ registrationStatus: "*Please provide correct infromation" });
            return;
          }
          else if (responseJson.ResponseStatus === "Success") {
            alert("success");
            this.setState({ registrationStatus: "Success" });

            this.props.navigation.navigate("Login");
            return responseJson.ResponseStatus;
          }

        })
        .catch(error => {
          console.error(error);
        });
    } else {
      this.setState({ registrationStatus: "", errMsg: "*All fields are required" });
    }


  }

  submit() {
    if (this.ValidateFields() === true) {
      const { navigation } = this.props;
      fetch(
        "http://flippoll.hackio.xyz:8080/api/UserRegistration/ValidateUserSecretQuestion",
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            UserName: this.state.phone,
            SecretQuestion: this.state.SecretQuestion,
            SecretAnswer: this.state.SecretAnswer,
          })
        }
      )
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.ResponseStatus === "NotFetched") {
            this.setState({ registrationStatus: "*Please provide correct infromation" });
            return;
          }
          else if (responseJson.ResponseStatus === "Success") {
            this.setState({ registrationStatus: "Success" });
            this.setState({ changeView: true });
            return responseJson.ResponseStatus;
          }

        })
        .catch(error => {
          console.error(error);
        });
    } else {
      this.setState({ registrationStatus: "", errMsg: "*All fields are required" });
    }


  }

  ValidatePassword(confirmPassword) {
    if (this.state.password === confirmPassword) {
      return true;
    }
    else {
      return false;
    }
  }



  render() {
    return (
      <View>
        {this.state.changeView === false ? (
          <View>
            <View style={styles.container}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  marginBottom: 10
                }}
              >
                <Text
                  style={{
                    color: "red",
                    paddingBottom: 5
                  }}
                >
                  {this.state.registrationStatus === "" ? this.state.errMsg : this.state.registrationStatus}
                </Text>
              </View>

              <View>

                <Text style={this.state.phone === "" && this.state.phone.length !== 11 ? [styles.hint, { color: "red" }] : styles.hint}>{this.state.phoneHint} </Text>

                <TextInput
                  style={styles.input}
                  onBlur={() => this._onBlur({ id: 0 })}
                  onFocus={() => this._onFocus({ id: 0 })}
                  underlineColorAndroid={this._getULColor(
                    this.state.hasFocus2
                  )}
                  placeholder="Phone Number"
                  placeholderTextColor="#0aae7e"
                  textColor="#27292b"
                  autoCapitalize="none"
                  onChangeText={text => this.toggleHint({ id: 0, text })}
                />

                <Text style={styles.hint}>{this.state.SecretQuestion + ""}</Text>

                <TextInput
                  style={styles.input}
                  underlineColorAndroid="#dbe2e0"
                  onBlur={() => this._onBlur({ id: 1 })}
                  onFocus={() => this._onFocus({ id: 1 })}
                  underlineColorAndroid={this._getULColor(
                    this.state.hasFocus5
                  )}
                  placeholder="Answer"
                  placeholderTextColor="#0aae7e"
                  autoCapitalize="none"
                  onChangeText={text => this.toggleHint({ id: 1, text })}
                />
                <TouchableOpacity
                  style={styles.submitButton}
                  onPress={() => this.submit()}
                >
                  <Text style={styles.submitButtonText}> CONFIRM </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        ) : (
            <View>
              <View style={styles.container}>
                <Text style={styles.hint}>{this.state.passwordHint} </Text>
                <TextInput
                  style={styles.input}
                  onBlur={() => this._onBlur({ id: 2 })}
                  onFocus={() => this._onFocus({ id: 2 })}
                  underlineColorAndroid={this._getULColor(
                    this.state.hasFocus4
                  )}
                  placeholder="Password"
                  placeholderTextColor="#0aae7e"
                  secureTextEntry={true}
                  textColor="#27292b"
                  autoCapitalize="none"
                  onChangeText={text => this.toggleHint({ id: 2, text })}
                />
                <Text style={!this.ValidatePassword(this.state.confirmPassword) ? [styles.hint, { color: "red" }] : styles.hint}>
                  {!this.ValidatePassword(this.state.confirmPassword) ? "Password Does Not Match" : this.state.confirmPasswordHint}
                </Text>
                <TextInput
                  style={styles.input}
                  onBlur={() => this._onBlur({ id: 3 })}
                  onFocus={() => this._onFocus({ id: 3 })}
                  underlineColorAndroid={this._getULColor(
                    this.state.hasFocus4
                  )}
                  placeholder="Confirm Password"
                  placeholderTextColor="#0aae7e"
                  secureTextEntry={true}
                  textColor="#27292b"
                  autoCapitalize="none"
                  onChangeText={text => this.toggleHint({ id: 3, text })}
                />
                <TouchableOpacity
                  style={styles.submitButton}
                  onPress={() => this.confirmChange()}
                >
                  <Text style={styles.submitButtonText}> SUBMIT </Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
      </View>
    );
  }
}


const styles = StyleSheet.create({
  submitButtonText: {
    color: "white",
    textAlign: "center",
    fontSize: 20
  },
  container: {
    margin: 40
  },
  hint: {
    color: "#0aae7e",
    fontSize: 14,
    fontFamily: "Roboto-Normal"
  },
  submitButton: {
    // flex: 1,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#0aae7e",
    height: 55,
    justifyContent: "center",
    borderRadius: 6,
    marginTop: 20
  },
});