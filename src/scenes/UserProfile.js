/**
 * Created By       : Sree Bijoy Kumar Mohanata
 * Date             : 24 July 2018
 * Purpose          : User Profile and Logout
 */
import AppContainer from "../styles/Container";
import AppStatusBar from "../components/AppStatusBar";
import React from "react";
import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  AsyncStorage,
  View,
  ScrollView
} from "react-native";
import Dimensions from "Dimensions";
import * as AppsColor from "../styles/Color";
import MapView from 'react-native-maps';
var { width, height } = Dimensions.get("window");

export default class UserProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: ""
    }
  }

  // @action: it navigates to Profile
  // @action: it calls api to logout from server
  funUserLogout = () => {
    AsyncStorage.removeItem('userObj').then(
      (response) => {

        // console.log("async_remove", response)
        this.props.navigation.navigate("Login")
      }
    )
  };
  setUserFullName(value) {
    value = JSON.parse(value)
    this.setState({
      username: value.FullName
    })
  }
  //for user name
  componentDidMount() {
    try {
      AsyncStorage.getItem("userObj")
        .then(value => {
          this.setUserFullName(value)
        }).done();
    }
    catch (error) {

    }
  }
  // @action: logouts user
  funSignout = () => {
    Alert.alert("Sign out", "Signing out your account?", [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      { text: "OK", onPress: this.funUserLogout }
    ]);
  };
  funChangePassword = () => {
    this.props.navigation.navigate("ChangePassword")
  };
  funSettings = () => {
    alert("Under Development")
  };
  funMyLocaton = () => {
    this.props.navigation.navigate("MyLocation")
  }
  render() {
    return (
      <View style={AppContainer.MainContainer}>
        {/* <AppStatusBar /> */}
        <MapView
          initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        />
        <ScrollView>
          <View
            style={{
              flex: 0,
              margin: 10,
              flexDirection: "column",
              alignItems: "center"
            }}
          >
            <View>
              <Image
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 50
                }}
                //source={{ uri: this.state.fbURL }}
                source={require("../assets/icons/avatar.png")}
              />
            </View>
            <View>
              <Text style={styles.textTitle}>{this.state.username}</Text>
              <Text style={styles.textSubTitle}>BRAC BANK LTD </Text>
            </View>
            <View
              style={{
                marginTop: 10,
                //margin: 5,
                alignSelf: "stretch",
                borderRadius: 6,
                flexDirection: "column",
                flex: 1,
                borderTopColor: "#bbbcc0",
                borderBottomColor: "#bbbcc0",
                backgroundColor: "#e7e7e7"
              }}
            >
              <TouchableOpacity onPress={this.funSignout} style={{
                margin: 5,
                alignSelf: "stretch",
                borderRadius: 6,
                flexDirection: "column",
                flex: 1,
                borderTopColor: "#bbbcc0",
                borderBottomColor: "#bbbcc0",
                backgroundColor: AppsColor.TAB_BAR_SELECTED_TINT_COLOR
              }}>
                <View style={{ flex: 0, flexDirection: "row", color: "#1d2438" }}>
                  <View>
                    <Image
                      style={{
                        width: 40,
                        height: 40,
                        margin: 10
                      }}
                      source={require("../assets/icons/signout.png")}
                    />
                  </View>
                  <View>
                    <Text style={styles.textCommonSignOut}>Sign out </Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.funChangePassword}>
                <View style={{
                  margin: 5,
                  alignSelf: "stretch",
                  borderRadius: 6,
                  flexDirection: "column",
                  flex: 1,
                  borderTopColor: "#bbbcc0",
                  borderBottomColor: "#bbbcc0",
                  backgroundColor: AppsColor.TAB_BAR_SELECTED_TINT_COLOR
                }}>
                  <View style={{ flex: 1, flexDirection: "row", color: "#1d2438" }}>
                    <View>
                      <Image
                        style={{
                          width: 50,
                          height: 50,
                          margin: 10
                        }}
                        source={require("../assets/icons/changepassword.png")}
                      />
                    </View>
                    <View>
                      <Text style={styles.textCommonSignOut}>Change Password </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.funSettings}>
                <View style={{
                  margin: 5,
                  alignSelf: "stretch",
                  borderRadius: 6,
                  flexDirection: "column",
                  flex: 1,
                  borderTopColor: "#bbbcc0",
                  borderBottomColor: "#bbbcc0",
                  backgroundColor: AppsColor.TAB_BAR_SELECTED_TINT_COLOR
                }}>
                  <View style={{ flex: 0, flexDirection: "row", color: "#1d2438" }}>
                    <View>
                      <Image
                        style={{
                          width: 50,
                          height: 50,
                          margin: 10
                        }}
                        source={require("../assets/icons/settings.png")}
                      />
                    </View>
                    <View>
                      <Text style={styles.textCommonSignOut}>Settings </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.funMyLocaton}>
                <View style={{
                  margin: 5,
                  alignSelf: "stretch",
                  borderRadius: 6,
                  flexDirection: "column",
                  flex: 1,
                  borderTopColor: "#bbbcc0",
                  borderBottomColor: "#bbbcc0",
                  backgroundColor: AppsColor.TAB_BAR_SELECTED_TINT_COLOR
                }}>
                  <View style={{ flex: 0, flexDirection: "row", color: "#1d2438" }}>
                    <View>
                      <Image
                        style={{
                          width: 50,
                          height: 50,
                          margin: 10
                        }}
                        source={require("../assets/icons/settings.png")}
                      />
                    </View>
                    <View>
                      <Text style={styles.textCommonSignOut}>My Location </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "stretch"
  },
  textCommon: {
    fontSize: 16,
    color: "#1d2438",
    textAlign: "center",
    padding: 15,
    justifyContent: "center",
    alignItems: "center",
    fontFamily: "Roboto-Medium"
  },
  textCommonSignOut: {
    fontSize: 20,
    color: "#fff",
    textAlign: "center",
    padding: 15,
    justifyContent: "center",
    alignItems: "center",
    fontFamily: "Roboto-Light"
  },
  textTitle: {
    fontSize: 19,
    color: "#1b1c1e",
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
    // fontWeight: "500",
    fontFamily: "Roboto-Medium"
  },
  textSubTitle: {
    fontSize: 15,
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
    color: "#006cb6",
    //fontWeight: "500",
    fontFamily: "Roboto-Medium"
  },
  textFirst2: {
    fontSize: 18.2,
    color: "#fff",
    textAlign: "center",
    padding: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  bottomText: {
    fontSize: 15,
    color: "#fff",
    textAlign: "center",
    padding: 10,
    justifyContent: "center",
    alignItems: "center"
  }
});

//export default MyProfile;
