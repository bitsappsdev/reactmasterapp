/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Created Date : 12 Jul 2018
 * Purpose      : Login Screen []
 ---- git 
 */
import Dimensions from "Dimensions";
import React from "react";
import { LoginButton, AccessToken } from 'react-native-fbsdk';
import { ToastAndroid, AsyncStorage, BackHandler, Platform, StyleSheet, Text, Alert, TextInput, TouchableOpacity, View } from "react-native";
import { getLogin } from "../api/FetchLogin";
import Toast from 'react-native-simple-toast';
import AppStatusBar from "../components/AppStatusBar";
import Logo from "../components/Logo";
import Wallpaper from "../components/Wallpaper";
import * as AppsColor from "../styles/Color";
import * as AppsString from "../styles/ConstantString";
import container from "../styles/Container";
import OfflineNotice from "../components/OfflineNotice"
import AppBar from "../components/AppBars";
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { Bubbles, DoubleBounce, Bars, Pulse } from 'react-native-loader';

// import Fabric from 'react-native-fabric';

// var { Crashlytics } = Fabric;

// Crashlytics.setUserName('megaman');

// Crashlytics.setUserEmail('bijoy.cse09@gmail.com');

// Crashlytics.setUserIdentifier('1234');

// Crashlytics.setBool('has_posted', true);

// Crashlytics.setString('organization', 'Acme. Corp');

// // Forces a native crash for testing
//Crashlytics.crash();
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // for response
      userauthenticationList: {},
      UserID: "",
      UserName: "",
      FullName: "",
      OrgName: "",
      Accesstoken: "",
      //for user end
      name: "",
      nameFormatValidate: true,
      password: "",
      passwordFormatValidate: true,
      errorMsg: "",
      isloading: true,
    };
  }

  // Somewhere in your code
  signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      this.setState({ userInfo });
      console.log("User Info :", userInfo);
      Toast.show("Login successful done")
    } catch (error) {
      console.log("Login error :", error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  goBack() {
    //this.props.navigation.navigate("Login");
    Alert.alert(
      'Exit App',
      'Do you want to exit?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false });
    return true;
  }

  handleBackPress = () => {
    this.goBack(); // works best when the goBack is async
    return true;
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  // componentDidMount() {

  // }

  componentDidMount() {
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: "519739307961-6h4kkt3t9uscfshungoap7v4lp61iqfp.apps.googleusercontent.com", // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      hostedDomain: '', // specifies a hosted domain restriction
      loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
      forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
      accountName: '', // [Android] specifies an account name on the device that should be used
      // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });

    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    // AsyncStorage.getItem('userObj').then(
    //   (response) => {
    //     if (response !== null) {
    //       this.props.navigation.navigate("Detail")
    //     }
    //     // console.log("async_remove", response)

    //   }
    // )
  }
  static navigationOptions = {
    header: null
  };
  handleLogin() {
    getLogin(this.state.name, this.state.password).then((res) => {
      if (res.Status === "Authorized") {
        this.setState({
          userauthenticationList: res,
          //isloading: false
        })
        AsyncStorage.setItem("userObj", JSON.stringify(this.state.userauthenticationList))
        Toast.show('You have logged in successfully.', Toast.SHORT);
        this.props.navigation.navigate("Detail");
      }
      else {
        this.setState({
          passwordFormatValidate: false,
          nameFormatValidate: false,
          errorMsg: "You have type wrong credential! try again.",
        });
        //Toast.show('You have type wrong credential! try again.', Toast.SHORT);
        //ToastAndroid.show('You have type wrong credential!!!', ToastAndroid.SHORT);
      }
    }).
      catch(error => {
        alert("Please check your internet connection & try again.");  //error        
      });
  }
  //Login
  _Login() {
    if (this.state.name.length == 0 || this.state.password.length == 0) {
      this.setState({
        errorMsg: "User name and password both are required.",
        passwordFormatValidate: false,
        nameFormatValidate: false
      });
    } else {
      if (this.state.name.length > 0 || this.state.password.length > 0) {
        // if (this.state.isloading) {
        //   return (
        //     <AppBar />
        //   );
        // }
        //this.handleLogin();
        getLogin(this.state.name, this.state.password).then((res) => {
          if (res.Status === "Authorized") {
            // this.setState({
            //   userauthenticationList: res,
            //   //isloading: false
            // })
            AsyncStorage.setItem("userObj", JSON.stringify(res))
              .then(() => {
                //console.log("It was saved successfully")
                Toast.show('You have logged in successfully.', Toast.SHORT);
                this.props.navigation.navigate("Detail");
              })
              .catch(() => {
                console.log("There was an error saving the userObj")
              })
            //AsyncStorage.setItem("userObj", res)

          }
          else {
            this.setState({
              passwordFormatValidate: false,
              nameFormatValidate: false,
              errorMsg: "You have type wrong credential! try again.",
            });
            //Toast.show('You have type wrong credential! try again.', Toast.SHORT);
            //ToastAndroid.show('You have type wrong credential!!!', ToastAndroid.SHORT);
          }
        }).
          catch(error => {
            alert("Please check your internet connection & try again.");  //error        
          });
      }
      else {
        Toast.show('You have type wrong credential!try again.', Toast.LONG);
        //ToastAndroid.show('You have type wrong credential!!!', ToastAndroid.SHORT);
      }
    }
  }
  //validate username and password
  validate(text, type) {
    if (type == "username") {
      if (text) {
        this.setState({
          nameFormatValidate: true,
          errorMsg: "",
          name: text
        });
      } else {
        this.setState({
          nameFormatValidate: false
        });
      }
    }
    if (type == "pass") {
      if (text) {
        this.setState({
          passwordFormatValidate: true,
          errorMsg: "",
          password: text
        });
      } else {
        this.setState({
          passwordFormatValidate: false
        });
      }
    }
  }
  render() {
    return (
      <View style={container.MainContainer}>
        <AppStatusBar />
        <OfflineNotice />
        <Wallpaper>
          <Logo />
          <View style={styles.inputWrapper}>
            <View>
              <GoogleSigninButton
                style={{ width: 48, height: 48 }}
                size={GoogleSigninButton.Size.Icon}
                color={GoogleSigninButton.Color.Dark}
                onPress={this.signIn}
                disabled={this.state.isSigninInProgress} />
            </View>
            <View>
              <LoginButton
                onLoginFinished={
                  (error, result) => {
                    if (error) {
                      console.log("login has error: " + result.error);
                    } else if (result.isCancelled) {
                      console.log("login is cancelled.");
                    } else {
                      AccessToken.getCurrentAccessToken().then(
                        (data) => {
                          console.log(data.accessToken.toString())
                        }
                      )
                    }
                  }
                }
                onLogoutFinished={() => console.log("logout.")} />
            </View>
            <View style={{ height: 60 }}>
              {Platform.OS === "ios" ? (
                <TextInput
                  style={styles.input}
                  borderBottomWidth={0.75}
                  borderBottomColor={
                    Platform.OS === "ios"
                      ? !this.state.nameFormatValidate
                        ? AppsColor.ERROR
                        : AppsColor.THEME_COLOR
                      : false
                  }
                  selectionColor={AppsColor.THEME_COLOR}
                  placeholder={AppsString.PLACEHOLDER_USER_NAME}
                  maxLength={AppsString.MAX_Length_USER_NAME}
                  placeholderTextColor={AppsColor.PLACRHOLDER_TEXT_COLOR}
                  onChangeText={text => this.validate(text, "username")}
                />
              ) : (
                  <TextInput
                    style={styles.input}
                    selectionColor={AppsColor.THEME_COLOR}
                    placeholder={AppsString.PLACEHOLDER_USER_NAME}
                    maxLength={AppsString.MAX_Length_USER_NAME}
                    placeholderTextColor={AppsColor.PLACRHOLDER_TEXT_COLOR}
                    underlineColorAndroid={
                      !this.state.nameFormatValidate
                        ? AppsColor.ERROR
                        : AppsColor.THEME_COLOR
                    }
                    onChangeText={text => this.validate(text, "username")}
                  />
                )}
            </View>
            <View style={{ marginLeft: -40 }}>
              <Text style={{ color: "#e53935" }}>{this.state.errorMsg}</Text>
            </View>
            <View style={{ height: 60 }}>
              {Platform.OS === "ios" ? (
                <TextInput
                  style={styles.input}
                  secureTextEntry={true}
                  borderBottomWidth={0.75}
                  borderBottomColor={
                    !this.state.passwordFormatValidate
                      ? AppsColor.ERROR
                      : AppsColor.THEME_COLOR
                  }
                  selectionColor={AppsColor.THEME_COLOR}
                  placeholder={AppsString.PLACEHOLDER_USER_PASSWORD}
                  maxLength={AppsString.MAX_Length_USER_PASSWORD}
                  placeholderTextColor={AppsColor.PLACRHOLDER_TEXT_COLOR}
                  onChangeText={text => this.validate(text, "pass")}
                />
              ) : (
                  <TextInput
                    style={styles.input}
                    secureTextEntry={true}
                    selectionColor={AppsColor.THEME_COLOR}
                    placeholder={AppsString.PLACEHOLDER_USER_PASSWORD}
                    maxLength={AppsString.MAX_Length_USER_PASSWORD}
                    placeholderTextColor={AppsColor.PLACRHOLDER_TEXT_COLOR}
                    underlineColorAndroid={
                      !this.state.passwordFormatValidate
                        ? AppsColor.ERROR
                        : AppsColor.THEME_COLOR
                    }
                    onChangeText={text => this.validate(text, "pass")}
                  />
                )}
            </View>
          </View>
          <View style={styles.container}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                this._Login();
              }}
            >
              <Text style={styles.text}>{AppsString.SUBMIT_BUTTON_TEXT}</Text>
            </TouchableOpacity>
          </View>

        </Wallpaper>
      </View>

    );
  }
}
export default Login;
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
//alert(DEVICE_HEIGHT);
const styles = StyleSheet.create({
  inputWrapper: {
    flex: 2,
    alignItems: "center"
  },
  input: {
    width: DEVICE_WIDTH - 40,
    fontSize: 18,
    color: AppsColor.TEXT_INPUT_COLOR,
    fontFamily: "Roboto-Light"
  },
  container: {
    flex: 1,
    //top: 30,
    justifyContent: "flex-end"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: AppsColor.THEME_COLOR,
    zIndex: 100,
    height: DEVICE_HEIGHT / 10.64
  },
  text: {
    fontSize: 18,
    //padding: 10,
    fontWeight: "500",
    color: AppsColor.SUBMIT_TEXT_COLOR
  },
  error: {
    borderWidth: 3,
    borderColor: "red"
  }
});
