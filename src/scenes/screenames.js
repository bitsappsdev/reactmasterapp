/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Purpose          : Screen Names
 * Date             : 12 Jul 2018
 */
export const SPLASH = "Splash";
export const LOGIN = "Login";
export const DETAIL = "Detail";
export const EXCHANGE_HOUSE = "Exchange Houses";
export const BBL_EXCHANGE_HOUSES = "BBL Branch Ex. Houses";
export const COUNTRY_ORIGIN = "Country of Origins";
export const BBL_BRANCE = "BBL Branches";
export const TRANSACTION_MODE = "Transaction Modes";
export const BAR_CHART = "Transaction Bar Charts";
export const DASH_BOARD = "Dashboard"
