/**
 * Screen Name   : Splash
 * Created By    : Sree Bijoy Kumar Mohata
 * Date          : 12 July 2018
 * Purpose       : Splash
 */

import React from "react";
import { ImageBackground, Image, View } from "react-native";
import styles from "../styles/Container";
import splash_backgroud from "../assets/images/splash_backgroud.png";
import masterapp_logo from "../assets/images/masterapp_logo_login.png";
import AppStatusBar from "../components/AppStatusBar";

class Splash extends React.Component {
  static navigationOptions = {
    header: null
  };
  UNSAFE_componentWillMount() {
    setTimeout(() => {
      this.props.navigation.navigate("Login");
    }, 1000);
  }
  render() {
    return (
      <ImageBackground style={styles.Background} source={splash_backgroud}>
        <AppStatusBar />
        <View style={styles.Center}>
          <Image source={masterapp_logo} style={{ width: 200, height: 50 }} />
        </View>
      </ImageBackground>
    );
  }
}

export default Splash;
