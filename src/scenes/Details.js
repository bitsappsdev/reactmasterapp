/*
Created By      : Sree Bijoy Kumar Mohanta
Date            : 23 july 2018
Purpose         : Master Detail 
*/
import React from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  FlatList,
  ScrollView,
  ToastAndroid,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import Toast from 'react-native-simple-toast';
import AppContainer from "../styles/Container";
import * as AppColor from "../styles/Color";
import Detail from "./Detail";
import AppBar from "../components/AppBars";
import { getNumberWithCommas } from "../api/Helpers";
import { getExchangeHouseDetails } from "../api/FetchExchangeHouse";
import { getCountryOriginDetails } from "../api/FetchCountryOfOrigin";
var randomColor = require("randomcolor");

const data = [
  {
    Key: 1,
    Prefix: "DC",
    ExchangeHouseName: "Direct Credit",
    NoOfTransaction: 1000,
    Amount: "1,522 BDT"
  },
  {
    Key: 2,
    Prefix: "SC",
    ExchangeHouseName: "Spot Cash",
    NoOfTransaction: 20,
    Amount: "1,225 BDT"
  },
  {
    Key: 3,
    Prefix: "EF",
    ExchangeHouseName: "EFT",
    NoOfTransaction: 12220,
    Amount: "15,022 BDT"
  },
  {
    Key: 4,
    Prefix: "BB",
    ExchangeHouseName: "bKash",
    NoOfTransaction: 1220,
    Amount: "10,522 BDT"
  }
];
class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      exchangeHouseList: [],
      _exID: "",
      _exPrefix: "",
      _exExHouseName: "",
      _exNoTrns: "",
      _exAmount: "",
      _exCurrency: "",
      _exColor: "",
      _exScreenId: "0",
      isRefresh: false,
      isloading: true,
    };
  }
  static navigationOptions = {
    headerTitle: "Exchange House",
    tabBarOptions: {
      labelStyle: {
        fontSize: 18,
        fontFamily: "Roboto-Black"
      }
    }
  };
  _funDetails(exchangehousename) {
    Toast.show(exchangehousename, Toast.LONG);
  }

  //list will be updated on pull to refresh
  onRefresh() {
    try {
      AsyncStorage.getItem("userObj")
        .then(value => {
          this.setState({
            isRefresh: true
          })
          this.handleExchangeHouseDetails(value)
        }).done();
    }
    catch (error) {

    }
  }
  handleExchangeHouseDetails(userObj) {
    userObj = JSON.parse(userObj);
    var startDate = new Date
    var endDate = new Date

    //startDate.toLocaleDateString('en-GB'), endDate.toLocaleDateString('en-GB')
    //alert(JSON.stringify(userObj.Accesstoken) + this.state._exID)
    switch (this.state._exScreenId) {
      case "0":
        getExchangeHouseDetails(this.state._exID, "10/10/2018", "10/20/2018", userObj.Accesstoken).then((res) => {
          //alert(JSON.stringify(res))
          if (res === "Access denied") {
            this.props.navigation.navigate("Login");
            Toast.show("Access denied. Session expired, please login again.", Toast.SHORT);
          } else {
            if (this.state.page === 1) {
              this.setState({
                exchangeHouseList: res,
                isloading: false,
                isRefresh: false
              })
            }
            else {
              this.setState({
                exchangeHouseList: [...this.state.exchangeHouseList, ...res],
                isloading: false,
                isRefresh: false
              })
            }
          }
        }).
          catch(error => {
            alert("Please check your internet connection & try again.");  //error        
          });
        break;
      case "1":
        getCountryOriginDetails(this.state._exID, "10/10/2018", "10/20/2018", userObj.Accesstoken).then((res) => {
          //alert(JSON.stringify(res))
          if (res === "Access denied") {
            this.props.navigation.navigate("Login");
            Toast.show("Access denied. Session expired, please login again.", Toast.SHORT);
          } else {
            if (this.state.page === 1) {
              this.setState({
                exchangeHouseList: res,
                isloading: false,
                isRefresh: false
              })
            }
            else {
              this.setState({
                exchangeHouseList: [...this.state.exchangeHouseList, ...res],
                isloading: false,
                isRefresh: false
              })
            }
          }
        }).
          catch(error => {
            alert("Please check your internet connection & try again.");  //error        
          });
        break;
      case "2":
        break;
      case "3":
        break;
    }

  }
  componentDidMount() {
    const { navigation } = this.props;
    _exID = navigation.getParam("exID", "N/A");
    _exPrefix = navigation.getParam("exPrefix", "N/A");
    _exExHouseName = navigation.getParam("exExHouseName", "N/A");
    _exNoTrns = navigation.getParam("exNoTrns", "N/A");
    _exAmount = navigation.getParam("exAmount", "N/A");
    _exCurrency = navigation.getParam("exCurrency", "N/A")
    _exScreenId = navigation.getParam("exScreenId", "N/A");
    this.setState({
      _exID: _exID,
      _exPrefix: _exPrefix,
      _exExHouseName: _exExHouseName,
      _exNoTrns: _exNoTrns,
      _exAmount: _exAmount,
      _exCurrency: _exCurrency,
      _exScreenId: _exScreenId
      //_exColor:__exColor
    });
    try {
      AsyncStorage.getItem("userObj")
        .then(value => {
          this.handleExchangeHouseDetails(value)
        }).done();
    }
    catch (error) {

    }
  }
  renderItem = ({ item, index }) => {
    switch (this.state._exScreenId) {
      case "0":
        return (
          <View
            style={{
              backgroundColor:
                index % 2 === 0
                  ? AppColor.LIST_EVEN_BKCOLOR
                  : AppColor.LIST_ODD_BKCOLOR
            }}
          >
            <TouchableOpacity onPress={() => this._funDetails(item.ExchangeHouseName)}>
              <View
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  flex: 1,
                  height: 77,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignContent: "center",
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    backgroundColor: randomColor(),
                    width: 40,
                    height: 40,
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 40
                  }}
                >
                  <Text
                    style={{
                      color: "#fff",
                      fontFamily: "Roboto-Bold",
                      fontSize: 16
                    }}
                  >
                    {/* {item.ExchangeHouseName.match(/[.*+?^${}()|[\]\\]/g, '\\$&').join('')} */}
                    {item.ExchangeHouseName.charAt(0)}
                  </Text>
                </View>

                <View
                  style={{
                    flex: 1,
                    marginLeft: 10,
                    flexDirection: "column",
                    justifyContent: "flex-start",
                    alignItems: "flex-start",
                    alignContent: "space-around"
                  }}
                >
                  <View>
                    <Text
                      style={{
                        //color: AppColor.LIST_FONT_COLOR_TITLE,
                        fontFamily: "Roboto-Medium",
                        fontSize: 14,
                        paddingBottom: 1,
                        color: "#141416"
                      }}
                    >
                      {item.ExchangeHouseName}
                    </Text>
                    <Text
                      style={{
                        //color: AppColor.LIST_FONT_COLOR_SUB_TITLE,
                        fontFamily: "Roboto-Regular",
                        fontSize: 12,
                        color: "#9d9d9d"
                      }}
                    >
                      No of Transactions:{item.TRAN_NO}
                    </Text>
                  </View>
                </View>
                <View style={{ marginVertical: 10 }}>
                  <Text
                    style={{
                      color: AppColor.LIST_LEFT_TITLE,
                      fontFamily: "Roboto-Bold",
                      fontSize: 14,
                      color: "#141416"
                    }}
                  >
                    {getNumberWithCommas(item.AMOUNT) + " " + (item.CURRENCY === null ? "" : item.CURRENCY)}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        );
        break;
      case "1":
        return (
          <View
            style={{
              backgroundColor:
                index % 2 === 0
                  ? AppColor.LIST_EVEN_BKCOLOR
                  : AppColor.LIST_ODD_BKCOLOR
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this._funDetails(
                  item.Name
                )
              }
            >
              <View
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  flex: 1,
                  height: 77,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignContent: "center",
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    backgroundColor: AppColor.FILTER_RADIO_BTN_COLOR, //randomColor(),
                    width: 40,
                    height: 40,
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 40
                  }}
                >
                  <Text
                    style={{
                      color: "#fff",
                      fontFamily: "Roboto-Bold",
                      fontSize: 16
                    }}
                  >
                    {item.Name.match(/\b(\w)/g).join('')}
                  </Text>
                </View>

                <View
                  style={{
                    flex: 1,
                    marginLeft: 10,
                    flexDirection: "column",
                    justifyContent: "flex-start",
                    alignItems: "flex-start",
                    alignContent: "space-around"
                  }}
                >
                  <View>
                    <Text
                      style={{
                        fontFamily: "Roboto-Medium",
                        fontSize: 14,
                        paddingBottom: 1,
                        color: "#141416"
                      }}
                    >
                      {item.Name}
                    </Text>
                    <Text
                      style={{
                        fontFamily: "Roboto-Regular",
                        fontSize: 12,
                        color: "#9d9d9d"
                      }}
                    >
                      No of Transactions:{item.TRAN_NO}
                    </Text>
                  </View>
                </View>
                <View style={{ marginVertical: 10 }}>
                  <Text
                    style={{
                      color: AppColor.LIST_LEFT_TITLE,
                      fontFamily: "Roboto-Bold",
                      fontSize: 14,
                      color: "#141416"
                    }}
                  >
                    {getNumberWithCommas(item.AMOUNT) + " " + (item.CURRENCY === null ? "" : item.CURRENCY)}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        );
        break;
      case "2":
        break;
    }

  };

  render() {
    // alert(this.state._exCurrency)
    if (this.state.isloading) {
      return (
        <AppBar />
      );
    }
    return (
      <View style={AppContainer.MainContainer}>
        <View style={AppContainer.DetailsHeader}>
          <View style={AppContainer.DetailsHeaderContent}>
            {this.state._exScreenId === "0" ? <View
              style={{
                backgroundColor: "#210000",
                margin: 5,
                width: 50,
                height: 50,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 50
              }}
            >
              <Text style={AppContainer.DetailsHeaderText2}>
                {/* {"ke".toUpperCase()} */}
                {this.state._exPrefix.toUpperCase()}
              </Text>
            </View> :
              <View
                style={{
                  backgroundColor: "#00852c",
                  margin: 5,
                  width: 50,
                  height: 50,
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 50
                }}
              >
                <Text style={AppContainer.DetailsHeaderText2}>
                  {/* {"ke".toUpperCase()} */}
                  {this.state._exPrefix.toUpperCase()}
                </Text>
              </View>
            }

            <Text style={AppContainer.DetailsHeaderText2}>
              {/* {"Khayer Exchange House".toString()} */}
              {this.state._exExHouseName.toString()}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              margin: 10,
              marginBottom: 10,
              flexDirection: "row",
              justifyContent: "space-evenly"
            }}
          >
            <View style={AppContainer.DetailsHeaderContent}>
              <Text style={AppContainer.DetailsHeaderText2}>
                {this.state._exNoTrns}
              </Text>
              <Text style={AppContainer.DetailsHeaderText1}>
                {"Transaction".toUpperCase()}
              </Text>
            </View>
            <View
              style={{
                borderLeftWidth: 1,
                width: 0.3,
                height: 47,
                borderLeftColor: "#9affbb"
              }}
            />
            <View style={AppContainer.DetailsHeaderContent}>
              <Text style={AppContainer.DetailsHeaderText2}>
                {getNumberWithCommas(this.state._exAmount)}
              </Text>
              {this.state._exCurrency === "" ? <Text style={AppContainer.DetailsHeaderText1}>
                AMOUNT{this.state._exCurrency.toUpperCase()}
              </Text> : <Text style={AppContainer.DetailsHeaderText1}>
                  AMOUNT({this.state._exCurrency.toUpperCase()})
              </Text>}

            </View>
          </View>
        </View>

        <View style={AppContainer.DetailsList}>
          <ScrollView>
            <FlatList data={this.state.exchangeHouseList} renderItem={(item, index) => this.renderItem(item, index)}
              onRefresh={() => this.onRefresh()}
              refreshing={this.state.isRefresh}
            />
          </ScrollView>
        </View>
      </View>
    );
  }
}
export default Details;
