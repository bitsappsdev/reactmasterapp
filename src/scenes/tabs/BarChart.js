/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Date             : 19 Jul 2018
 * Purpose          : TAB Bar Chart Scenes
 */
import React from "react";
import { View, Image, StyleSheet, Text } from "react-native";
import * as ScreenName from "../screenames";
import BarChartTab from "../../navigation/barchartnavigations";
import CommonHeader from "../../components/CommonHeader"
class BarChart extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <CommonHeader navigation={this.props.navigation} />
        <View style={{ marginTop: 10 }} />
        <View
          style={{
            alignItems: "flex-start",
            margin: 10
          }}
        >
          <Text
            style={{
              color: "#1b1c1e",
              fontSize: 27,
              fontFamily: "Roboto-Black"
            }}
          >
            {ScreenName.BAR_CHART}
          </Text>
        </View>
        <View
          style={{
            borderBottomColor: "#e7e7e7",
            borderBottomWidth: 0.4,
            marginLeft: 60
          }}
        />
        <BarChartTab />
      </View>
    );
  }
}
export default BarChart;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "stretch",
    width: null,
    paddingTop: 0,
    backgroundColor: "#ffffff"
  }
});
