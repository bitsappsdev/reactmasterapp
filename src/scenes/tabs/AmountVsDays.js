/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Date             : 19 Jul 2018
 * Purpose          : TAB Bar Chart Scenes
 */
import React from "react";
import { View } from "react-native";
import { VictoryAxis, VictoryBar, VictoryChart, VictoryLabel, VictoryLine } from "victory-native";
import * as AppColor from "..//../styles/Color";
import { getAmountVsDaysList } from "../../api/FetchBarCharAmountVsDays";
import AppBar from "../../components/AppBars";
import { AsyncStorage } from "react-native"
import Toast from 'react-native-simple-toast';
var dateFormat = require('dateformat');
class AmountVsDays extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      amountVsDaysList: [],
      isloading: true,
    }
  }

  handleAmountVsDaysList(userObj) {
    userObj = JSON.parse(userObj);
    getAmountVsDaysList(1, "10/10/2018", "10/20/2018", userObj.Accesstoken).then((res) => {
      if (res === "Access denied") {
        this.props.navigation.navigate("Login");
        Toast.show("Access denied. Session expired, please login again.", Toast.SHORT);
      } else {
        this.setState({
          amountVsDaysList: res,
          isloading: false,
        })
      }
    })
  }

  componentDidMount() {
    this.load()
    this.props.navigation.addListener('willFocus', this.load)
    try {
      AsyncStorage.getItem("userObj")
        .then(value => {
          this.handleAmountVsDaysList(value)
        }).done();
    }
    catch (error) {

    }
  }

  load = () => {
    this.setState({
      isloading: true
    });

    if (this.state.isLoader) {
      return (
        <AppBar />
      );
    }

    try {
      AsyncStorage.getItem("userObj")
        .then(value => {
          this.handleAmountVsDaysList(value)
        }).done();
    }
    catch (error) {

    }
  }
  render() {
    if (this.state.isloading) {
      return (
        <AppBar />
      );
    }
    return (
      <View style={{ padding: 0 }}>
        <VictoryChart padding={{ top: 30, bottom: 70, left: 80, right: 30 }} domain={{ x: [0, 7] }}
          animate={{ duration: 0 }}>
          <VictoryAxis
            label="Amount"
            style={{
              axis: { stroke: "#e2e2e8", strokeWidth: 0, padding: 0 },
              axisLabel: { fontSize: 20, fill: "#bbbbc1", padding: 55 },
              ticks: { stroke: "#e2e2e8", strokeWidth: 0 }
            }}
            dependentAxis
            tickFormat={(x) => (`${x / 1000000}M`)}
          />
          <VictoryAxis
            label="Days"
            style={{
              axis: { stroke: "#e2e2e8" },
              axisLabel: { fontSize: 20, padding: 50, fill: "#bbbbc1" },
              ticks: { stroke: "#e2e2e8", strokeWidth: 0 },
              tickLabels: {
                fontSize: 14, padding: 5,
                fontFamily: "Roboto-Black",
                fill: AppColor.THEME_COLOR
              }

            }}
          />
          <VictoryBar
            style={{
              data: {
                fill: AppColor.THEME_COLOR,
                width: 30,
                fillOpacity: 0.7,
                strokeWidth: 1
              }
            }}
            data={this.state.amountVsDaysList}
            x={(d) => dateFormat(d.TransactionDate, "d") + "\n" + dateFormat(d.TransactionDate, "ddd")}
            y={(d) => (d.AMOUNT)}
          />
        </VictoryChart>
      </View>
    );
  }
}
export default AmountVsDays;
