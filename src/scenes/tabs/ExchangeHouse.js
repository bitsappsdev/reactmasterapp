/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Date             : 19 Jul 2018
 * Purpose          : TAB Exchange House Scenes
 */
import React from "react";
import {
  FlatList, Text, TouchableOpacity, View, Dimensions,
  Animated, StyleSheet, AsyncStorage
} from "react-native";
import { getExchangeHouseList } from "../../api/FetchExchangeHouse";
import { getNumberWithCommas } from "../../api/Helpers";
import AppBar from "../../components/AppBars";
import * as AppColor from "../../styles/Color";
import * as ScreenName from "../screenames";
import CommonHeader from "../../components/CommonHeader"
import Toast from 'react-native-simple-toast';
import { YellowBox } from 'react-native';
//Removing the warnings
//console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];
console.disableYellowBox = true;

var randomColor = require("randomcolor"); // import the script
class ExchangeHouse extends React.Component {
  constructor(props) {
    super(props);
    this.offset = 0;
    this.state = {
      exchangeHouseList: [],
      isloading: true,
      page: 1,
      isRefresh: false,
      scrollOffset: new Animated.Value(0),
      titleWidth: 0,
    }
  }
  _funDetails(id, prefix, exchangehousename, nooftransaction, amount, currency) {
    //alert(prefix)
    this.props.navigation.navigate("Details", {
      exID: id,
      exPrefix: prefix,
      exExHouseName: exchangehousename,
      exNoTrns: nooftransaction,
      exAmount: amount,
      exCurrency: currency,
      exScreenId: "0"
    });
  }

  handleExchangeHouseList(userObj) {
    userObj = JSON.parse(userObj);
    var startDate = new Date
    var endDate = new Date
    //1 / 10 / 2018
    //startDate.toLocaleDateString('en-GB')
    //endDate.toLocaleDateString('en-GB')
    //alert(JSON.stringify(userObj.Accesstoken) + startDate + endDate)
    //alert(this.state.page)
    getExchangeHouseList(this.state.page, "10/10/2018", "10/20/2018", userObj.Accesstoken).then((res) => {
      //alert(JSON.stringify(res))
      if (res === "Access denied") {
        this.props.navigation.navigate("Login");
        Toast.show("Access denied. Session expired, please login again.", Toast.SHORT);
      } else {
        if (this.state.page === 1) {
          this.setState({
            exchangeHouseList: res,
            isloading: false,
            isRefresh: false
          })
        }
        else {
          this.setState({
            exchangeHouseList: [...this.state.exchangeHouseList, ...res],
            isloading: false,
            isRefresh: false
          })
        }
      }
    }).
      catch(error => {
        //alert("Please check your internet connection & try again.");  //error    
        this.props.navigation.navigate("Login");
      });
  }
  static navigationOptions = {
    header: null
  };
  //onscroll
  onScroll = e => {
    const scrollSensitivity = 4 / 3;
    const offset = e.nativeEvent.contentOffset.y / scrollSensitivity;
    this.state.scrollOffset.setValue(offset);
  };
  //call the api and recods from server
  componentDidMount() {
    try {
      AsyncStorage.getItem("userObj")
        .then(value => {
          this.handleExchangeHouseList(value)
        }).done();
    }
    catch (error) {

    }
    this.state.scrollOffset.addListener(({ value }) => (this.offset = value));
  }
  //list will be updated on pull to refresh
  onRefresh() {
    try {
      AsyncStorage.getItem("userObj")
        .then(value => {
          this.setState({ isRefresh: true, page: 1 })
          this.handleExchangeHouseList(value)
        }).done();
    }
    catch (error) {

    }
    //this.setState({ isRefresh: true, page: 1 }, function () { this.handleExchangeHouseList() });
  }

  handleloadMore() {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        //console.log("page2", this.state.page)
        try {
          AsyncStorage.getItem("userObj")
            .then(value => {
              //this.setState({ isRefresh: true, page: 1 })
              this.handleExchangeHouseList(value)
            }).done();
        }
        catch (error) {

        }
        // this.handleExchangeHouseList();
      }
    );
  };

  renderItem = ({ item, index }) => {
    return (
      <View
        style={{
          backgroundColor:
            index % 2 === 0
              ? AppColor.LIST_EVEN_BKCOLOR
              : AppColor.LIST_ODD_BKCOLOR
        }}
      >
        <TouchableOpacity
          onPress={() =>
            this._funDetails(
              item.Id,
              // item.ExchangeHouseName.match(/[.*+?^${}()|[\]\\]/g, '\\$&').join(''),
              item.ExchangeHouseName.charAt(0),
              item.ExchangeHouseName,
              item.TRAN_NO,
              item.AMOUNT,
              item.CURRENCY === null ? "" : item.CURRENCY
            )
          }
        >
          <View
            style={{
              marginLeft: 10,
              marginRight: 10,
              flex: 1,
              height: 77,
              flexDirection: "row",
              justifyContent: "center",
              alignContent: "center",
              alignItems: "center"
            }}
          >
            <View
              style={{
                backgroundColor: AppColor.FILTER_RADIO_BTN_COLOR, //randomColor(),
                width: 40,
                height: 40,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 40
              }}
            >
              <Text
                style={{
                  color: "#fff",
                  fontFamily: "Roboto-Bold",
                  fontSize: 16
                }}
              >
                {/* {item.ExchangeHouseName.match(/\b(\w)/g).join('')} */}
                {item.ExchangeHouseName.charAt(0)}
              </Text>
            </View>

            <View
              style={{
                flex: 1,
                marginLeft: 10,
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "flex-start",
                alignContent: "space-around"
              }}
            >
              <View>
                <Text
                  style={{
                    fontFamily: "Roboto-Medium",
                    fontSize: 14,
                    paddingBottom: 1,
                    color: "#141416"
                  }}
                >
                  {item.ExchangeHouseName}
                </Text>
                <Text
                  style={{
                    fontFamily: "Roboto-Regular",
                    fontSize: 12,
                    color: "#9d9d9d"
                  }}
                >
                  No of Transactions:{item.TRAN_NO}
                </Text>
              </View>
            </View>
            <View style={{ marginVertical: 10 }}>
              <Text
                style={{
                  color: AppColor.LIST_LEFT_TITLE,
                  fontFamily: "Roboto-Bold",
                  fontSize: 14,
                  color: "#141416"
                }}
              >
                {getNumberWithCommas(item.AMOUNT) + " " + (item.CURRENCY === null ? "" : item.CURRENCY)}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    const { navigate } = this.props.navigation;
    console.log("EXH", this.state.isloading);
    const { scrollOffset } = this.state;
    const screenWidth = Dimensions.get('window').width;
    if (this.state.isloading) {
      return (
        <AppBar />
      );
    }
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#ffffff",
        }}
      >
        <CommonHeader navigation={this.props.navigation} />
        <Animated.View
          style={[
            styles.header,
            {
              backgroundColor: "#ffffff",
              paddingHorizontal: screenWidth * 0.05,
              width: screenWidth,
              height: scrollOffset.interpolate({
                inputRange: [0, 160],
                outputRange: [80, 37],
                extrapolate: 'clamp',
              }),
            },
          ]}>
          <Animated.Text
            onLayout={e => {
              if (this.offset === 0 && this.state.titleWidth === 0) {
                const titleWidth = e.nativeEvent.layout.width;
                this.setState({ titleWidth });
              }
            }}
            style={{
              color: "#1b1c1e",
              fontSize: 27,
              paddingBottom: 7,
              fontFamily: "Roboto-Black",
              fontSize: scrollOffset.interpolate({
                inputRange: [0, 200],
                outputRange: [26, 20],
                extrapolate: 'clamp',
              }),
            }}>
            {ScreenName.DASH_BOARD}
          </Animated.Text>
          <View style={{ position: 'absolute', right: 10 }}>
            <Text style={{
              color: "#9d9d9d",
              fontSize: 15,
              paddingBottom: 20,
              fontFamily: "Roboto-Medium"
            }}>

            </Text>
          </View>
          <Animated.View
            style={{
              width: scrollOffset.interpolate({
                inputRange: [0, 100],
                outputRange: [screenWidth * 0.9 - this.state.titleWidth, 0],
                extrapolate: 'clamp',
              }),
            }}
          />
        </Animated.View>
        <View
          style={{
            borderBottomColor: "#e7e7e7",
            borderBottomWidth: 0.4,
            marginLeft: 60
          }}
        />
        <View
          style={{
            flex: 0.60,
            margin: 10,
            backgroundColor: AppColor.THEME_COLOR,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10
          }}
        >
          <Text style={{ fontSize: 36, color: "#fff", fontFamily: "Roboto-Bold" }}>34,33,434$</Text>
          <Text style={{ fontSize: 22, color: "#fff", fontFamily: "Roboto-Light" }}>Total Transactions</Text>
        </View>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <View style={{
              margin: 10,
              borderRadius: 10,
              marginBottom: 5,
              borderColor: "#e6e6e6",
              backgroundColor: "#ffffff",
              width: 160.7,
              borderWidth: 0.5,
              height: 100.7,
            }}>
              <Text style={{ fontSize: 22, color: AppColor.THEME_COLOR, fontFamily: "Roboto-Bold", paddingTop: 10, paddingLeft: 10 }}>34,33,434$</Text>
              <Text style={{ fontSize: 16, color: AppColor.TAB_BAR_SELECTED_TINT_COLOR, fontFamily: "Roboto-Light", paddingLeft: 10 }}>Exchnage House</Text>
              <Text style={{ fontSize: 12, color: AppColor.TAB_BAR_SELECTED_TINT_COLOR, fontFamily: "Roboto-Light", paddingLeft: 10, marginTop: 10 }}>View Detail --></Text>
            </View>
            <View style={{
              margin: 10,
              borderRadius: 10,
              marginBottom: 5,
              borderColor: "#e6e6e6",
              backgroundColor: "#ffffff",
              width: 160.7,
              borderWidth: 0.5,
              height: 100.7,
            }}>
              <Text style={{ fontSize: 22, color: AppColor.THEME_COLOR, fontFamily: "Roboto-Bold", paddingTop: 10, paddingLeft: 10 }}>34,33,434$</Text>
              <Text style={{ fontSize: 16, color: AppColor.TAB_BAR_SELECTED_TINT_COLOR, fontFamily: "Roboto-Light", paddingLeft: 10 }}>BBL Branch</Text>
              <Text style={{ fontSize: 12, color: AppColor.TAB_BAR_SELECTED_TINT_COLOR, fontFamily: "Roboto-Light", paddingLeft: 10, marginTop: 10 }}>View Detail --></Text>
            </View>
          </View>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'stretch',
          }}>
            <View style={{
              margin: 10,
              borderRadius: 10,
              marginBottom: 5,
              borderColor: "#e6e6e6",
              backgroundColor: "#ffffff",
              width: 160.7,
              borderWidth: 0.5,
              height: 100.7,
            }}>
              <Text style={{ fontSize: 22, color: AppColor.THEME_COLOR, fontFamily: "Roboto-Bold", paddingTop: 10, paddingLeft: 10 }}>34,33,434$</Text>
              <Text style={{ fontSize: 16, color: AppColor.TAB_BAR_SELECTED_TINT_COLOR, fontFamily: "Roboto-Light", paddingLeft: 10 }}>Country</Text>
              <Text style={{ fontSize: 12, color: AppColor.TAB_BAR_SELECTED_TINT_COLOR, fontFamily: "Roboto-Light", paddingLeft: 10, marginTop: 10 }}>View Detail --></Text>
            </View>
            <View style={{
              margin: 10,
              borderRadius: 10,
              marginBottom: 5,
              borderColor: "#e6e6e6",
              backgroundColor: "#ffffff",
              width: 160.7,
              borderWidth: 0.5,
              height: 100.7,
            }} >
              <Text style={{ fontSize: 22, color: AppColor.THEME_COLOR, fontFamily: "Roboto-Bold", paddingTop: 10, paddingLeft: 10 }}>34,33,434$</Text>
              <Text style={{ fontSize: 16, color: AppColor.TAB_BAR_SELECTED_TINT_COLOR, fontFamily: "Roboto-Light", paddingLeft: 10 }}>Exchange House</Text>
              <Text style={{ fontSize: 12, color: AppColor.TAB_BAR_SELECTED_TINT_COLOR, fontFamily: "Roboto-Light", paddingLeft: 10, marginTop: 10 }}>View Detail --></Text>
            </View>
          </View>
        </View>
        {/* <FlatList
          style={{ width: '100%', flexGrow: 1, marginBottom: 0 }}//90
          data={this.state.exchangeHouseList}
          onScroll={this.onScroll}
          renderItem={(item, index) => this.renderItem(item, index)}
          keyExtractor={item => item.Id}
          onRefresh={() => this.onRefresh()}
          refreshing={this.state.isRefresh}
          onEndReached={({ distanceFromEnd }) => {
            this.handleloadMore();
          }}
          onEndReachedThreshold={0.5}
        /> */}
      </View>
    );
  }
}
export default ExchangeHouse;
const styles = StyleSheet.create({
  header: {
    backgroundColor: 'whitesmoke',
    borderBottomWidth: 1,
    borderColor: 'gainsboro',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingBottom: 8,
  },
  container: {
    flex: 1,
    borderWidth: 3,
    borderColor: 'green',
    flexDirection: 'column',
    paddingTop: 10

  },
  wordContainer: {
    flexDirection: 'row',
    borderColor: 'blue',
    borderWidth: 2
  },
  item2: {
    borderColor: 'black',
    borderWidth: 2,
    flexDirection: 'column',
  }
});