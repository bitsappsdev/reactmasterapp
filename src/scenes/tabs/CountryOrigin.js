/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Date             : 19 Jul 2018
 * Purpose          : TAB Country of Origin Scenes
 */
import React from "react";
import { FlatList, Image, Text, TouchableOpacity, View, Button, Animated, StyleSheet, Dimensions, AsyncStorage } from "react-native";
import { getCountryOriginList } from "../../api/FetchCountryOfOrigin";
import { getNumberWithCommas } from "../../api/Helpers";
import AppBar from "../../components/AppBars";
import CommonHeader from "../../components/CommonHeader";
import * as AppColor from "../../styles/Color.js";
import * as ScreenName from "../screenames";
import Toast from 'react-native-simple-toast';
import Swiper from "react-native-deck-swiper";
//const getCountryISO2 = require("country-iso-3-to-2");

var countryISOMapping = {
  AFG: "AF",
  ALA: "AX",
  ALB: "AL",
  DZA: "DZ",
  ASM: "AS",
  AND: "AD",
  AGO: "AO",
  AIA: "AI",
  ATA: "AQ",
  ATG: "AG",
  ARG: "AR",
  ARM: "AM",
  ABW: "AW",
  AUS: "AU",
  AUT: "AT",
  AZE: "AZ",
  BHS: "BS",
  BHR: "BH",
  BGD: "BD",
  BRB: "BB",
  BLR: "BY",
  BEL: "BE",
  BLZ: "BZ",
  BEN: "BJ",
  BMU: "BM",
  BTN: "BT",
  BOL: "BO",
  BIH: "BA",
  BWA: "BW",
  BVT: "BV",
  BRA: "BR",
  VGB: "VG",
  IOT: "IO",
  BRN: "BN",
  BGR: "BG",
  BFA: "BF",
  BDI: "BI",
  KHM: "KH",
  CMR: "CM",
  CAN: "CA",
  CPV: "CV",
  CYM: "KY",
  CAF: "CF",
  TCD: "TD",
  CHL: "CL",
  CHN: "CN",
  HKG: "HK",
  MAC: "MO",
  CXR: "CX",
  CCK: "CC",
  COL: "CO",
  COM: "KM",
  COG: "CG",
  COD: "CD",
  COK: "CK",
  CRI: "CR",
  CIV: "CI",
  HRV: "HR",
  CUB: "CU",
  CYP: "CY",
  CZE: "CZ",
  DNK: "DK",
  DJI: "DJ",
  DMA: "DM",
  DOM: "DO",
  ECU: "EC",
  EGY: "EG",
  SLV: "SV",
  GNQ: "GQ",
  ERI: "ER",
  EST: "EE",
  ETH: "ET",
  FLK: "FK",
  FRO: "FO",
  FJI: "FJ",
  FIN: "FI",
  FRA: "FR",
  GUF: "GF",
  PYF: "PF",
  ATF: "TF",
  GAB: "GA",
  GMB: "GM",
  GEO: "GE",
  DEU: "DE",
  GHA: "GH",
  GIB: "GI",
  GRC: "GR",
  GRL: "GL",
  GRD: "GD",
  GLP: "GP",
  GUM: "GU",
  GTM: "GT",
  GGY: "GG",
  GIN: "GN",
  GNB: "GW",
  GUY: "GY",
  HTI: "HT",
  HMD: "HM",
  VAT: "VA",
  HND: "HN",
  HUN: "HU",
  ISL: "IS",
  IND: "IN",
  IDN: "ID",
  IRN: "IR",
  IRQ: "IQ",
  IRL: "IE",
  IMN: "IM",
  ISR: "IL",
  ITA: "IT",
  JAM: "JM",
  JPN: "JP",
  JEY: "JE",
  JOR: "JO",
  KAZ: "KZ",
  KEN: "KE",
  KIR: "KI",
  PRK: "KP",
  KOR: "KR",
  KWT: "KW",
  KGZ: "KG",
  LAO: "LA",
  LVA: "LV",
  LBN: "LB",
  LSO: "LS",
  LBR: "LR",
  LBY: "LY",
  LIE: "LI",
  LTU: "LT",
  LUX: "LU",
  MKD: "MK",
  MDG: "MG",
  MWI: "MW",
  MYS: "MY",
  MDV: "MV",
  MLI: "ML",
  MLT: "MT",
  MHL: "MH",
  MTQ: "MQ",
  MRT: "MR",
  MUS: "MU",
  MYT: "YT",
  MEX: "MX",
  FSM: "FM",
  MDA: "MD",
  MCO: "MC",
  MNG: "MN",
  MNE: "ME",
  MSR: "MS",
  MAR: "MA",
  MOZ: "MZ",
  MMR: "MM",
  NAM: "NA",
  NRU: "NR",
  NPL: "NP",
  NLD: "NL",
  ANT: "AN",
  NCL: "NC",
  NZL: "NZ",
  NIC: "NI",
  NER: "NE",
  NGA: "NG",
  NIU: "NU",
  NFK: "NF",
  MNP: "MP",
  NOR: "NO",
  OMN: "OM",
  PAK: "PK",
  PLW: "PW",
  PSE: "PS",
  PAN: "PA",
  PNG: "PG",
  PRY: "PY",
  PER: "PE",
  PHL: "PH",
  PCN: "PN",
  POL: "PL",
  PRT: "PT",
  PRI: "PR",
  QAT: "QA",
  REU: "RE",
  ROU: "RO",
  RUS: "RU",
  RWA: "RW",
  BLM: "BL",
  SHN: "SH",
  KNA: "KN",
  LCA: "LC",
  MAF: "MF",
  SPM: "PM",
  VCT: "VC",
  WSM: "WS",
  SMR: "SM",
  STP: "ST",
  SAU: "SA",
  SEN: "SN",
  SRB: "RS",
  SYC: "SC",
  SLE: "SL",
  SGP: "SG",
  SVK: "SK",
  SVN: "SI",
  SLB: "SB",
  SOM: "SO",
  ZAF: "ZA",
  SGS: "GS",
  SSD: "SS",
  ESP: "ES",
  LKA: "LK",
  SDN: "SD",
  SUR: "SR",
  SJM: "SJ",
  SWZ: "SZ",
  SWE: "SE",
  CHE: "CH",
  SYR: "SY",
  TWN: "TW",
  TJK: "TJ",
  TZA: "TZ",
  THA: "TH",
  TLS: "TL",
  TGO: "TG",
  TKL: "TK",
  TON: "TO",
  TTO: "TT",
  TUN: "TN",
  TUR: "TR",
  TKM: "TM",
  TCA: "TC",
  TUV: "TV",
  UGA: "UG",
  UKR: "UA",
  ARE: "AE",
  GBR: "GB",
  USA: "US",
  UMI: "UM",
  URY: "UY",
  UZB: "UZ",
  VUT: "VU",
  VEN: "VE",
  VNM: "VN",
  VIR: "VI",
  WLF: "WF",
  ESH: "EH",
  YEM: "YE",
  ZMB: "ZM",
  ZWE: "ZW"
}
var countryColorMapping = {
  AFG: "#00AA46",
  ALA: "#EB0000",
  ALB: "#00A455",
  ASM: "AS",
  AND: "AD",
  AGO: "AO",
  AIA: "AI",
  ATA: "AQ",
  ATG: "#000701",
  ARG: "#7B9DDF",
  ARM: "AM",
  ABW: "AW",
  AUS: "#33008D",
  AUT: "#F20000",
  AZE: "AZ",
  BHS: "#00D2CE",
  BHR: "#E30000",
  BGD: "#009259",
  BRB: "BB",
  BLR: "BY",
  BEL: "BE",
  BLZ: "BZ",
  BEN: "BJ",
  BMU: "BM",
  BTN: "BT",
  BOL: "BO",
  BIH: "BA",
  BWA: "BW",
  BVT: "BV",
  BRA: "#F5F500",
  VGB: "VG",
  IOT: "IO",
  BRN: "#EAFF00",
  BGR: "BG",
  BFA: "BF",
  BDI: "BI",
  KHM: "KH",
  CMR: "CM",
  CAN: "#FF0000",
  CPV: "CV",
  CYM: "KY",
  CAF: "CF",
  TCD: "TD",
  CHL: "#3800A8",
  CHN: "#EA0000",
  HKG: "HK",
  MAC: "MO",
  CXR: "CX",
  CCK: "CC",
  COL: "CO",
  COM: "KM",
  COG: "CG",
  COD: "CD",
  COK: "CK",
  CRI: "CR",
  CIV: "CI",
  HRV: "HR",
  CUB: "CU",
  CYP: "#F3B700",
  CZE: "CZ",
  DNK: "#D80000",
  DJI: "#6AB1EC",
  DMA: "DM",
  DOM: "DO",
  ECU: "EC",
  EGY: "#E00000",
  SLV: "SV",
  GNQ: "GQ",
  ERI: "ER",
  EST: "EE",
  ETH: "ET",
  FLK: "FK",
  FRO: "FO",
  FJI: "FJ",
  FIN: "#2C3E94",
  FRA: "#360096",
  GUF: "GF",
  PYF: "PF",
  ATF: "TF",
  GAB: "GA",
  GMB: "GM",
  GEO: "GE",
  DEU: "DE",
  GHA: "GH",
  GIB: "GI",
  GRC: "#3B2BC7",
  GRL: "GL",
  GRD: "GD",
  GLP: "GP",
  GUM: "GU",
  GTM: "GT",
  GGY: "GG",
  GIN: "GN",
  GNB: "GW",
  GUY: "GY",
  HTI: "HT",
  HMD: "HM",
  VAT: "VA",
  HND: "HN",
  HUN: "HU",
  ISL: "IS",
  IND: "#FF8900",
  IDN: "#DA0000",
  IRN: "#00B044",
  IRQ: "#E40000",
  IRL: "#3F00BD",
  IMN: "IM",
  ISR: "IL",
  ITA: "IT",
  JAM: "#00AC40",
  JPN: "#FC0000",
  JEY: "JE",
  JOR: "#E30000",
  KAZ: "#25AEEA",
  KEN: "#000701",
  KIR: "KI",
  PRK: "KP",
  KOR: "KR",
  KWT: "#00AF42",
  KGZ: "KG",
  LAO: "LA",
  LVA: "LV",
  LBN: "LB",
  LSO: "LS",
  LBR: "LR",
  LBY: "LY",
  LIE: "LI",
  LTU: "LT",
  LUX: "LU",
  MKD: "MK",
  MDG: "MG",
  MWI: "MW",
  MYS: "MY",
  MDV: "MV",
  MLI: "#00B25C",
  MLT: "#E90018",
  MHL: "MH",
  MTQ: "MQ",
  MRT: "MR",
  MUS: "MU",
  MYT: "YT",
  MEX: "#00714B",
  FSM: "FM",
  MDA: "#3C00B0",
  MCO: "#E50000",
  MNG: "#D40000",
  MNE: "ME",
  MSR: "MS",
  MAR: "MA",
  MOZ: "MZ",
  MMR: "MM",
  NAM: "#310096",
  NRU: "#E50000",
  NPL: "NP",
  NLD: "#D50000",
  ANT: "AN",
  NCL: "NC",
  NZL: "#2A007E",
  NIC: "NI",
  NER: "#D50000",
  NGA: "#00A16A",
  NIU: "NU",
  NFK: "NF",
  MNP: "MP",
  NOR: "#E50000",
  OMN: "OM",
  PAK: "PK",
  PLW: "PW",
  PSE: "PS",
  PAN: "PA",
  PNG: "PG",
  PRY: "PY",
  PER: "PE",
  PHL: "PH",
  PCN: "PN",
  POL: "#EF0000",
  PRT: "#006F3A",
  PRI: "PR",
  QAT: "#AC003B",
  REU: "RE",
  ROU: "RO",
  RUS: "RU",
  RWA: "RW",
  BLM: "BL",
  SHN: "SH",
  KNA: "KN",
  LCA: "LC",
  MAF: "MF",
  SPM: "PM",
  VCT: "VC",
  WSM: "WS",
  SMR: "#65ABE7",
  STP: "#00BD67",
  SAU: "#00A555",
  SEN: "#00A360",
  SRB: "RS",
  SYC: "SC",
  SLE: "SL",
  SGP: "#E90000",
  SVK: "SK",
  SVN: "SI",
  SLB: "SB",
  SOM: "SO",
  ZAF: "ZA",
  SGS: "GS",
  SSD: "SS",
  ESP: "ES",
  LKA: "LK",
  SDN: "#E00000",
  SUR: "SR",
  SJM: "SJ",
  SWZ: "#EC0000",
  SWE: "#2B55AA",
  CHE: "#3800A8",
  SYR: "#D50000",
  TWN: "#3C009C",
  TJK: "#E20000",
  TZA: "#00C855",
  THA: "#5300C8",
  TLS: "#FB0000",
  TGO: "#00A468",
  TKL: "TK",
  TON: "#D50000",
  TTO: "#EC0000",
  TUN: "#E60000",
  TUR: "#E60000",
  TKM: "#80A782",
  TCA: "TC",
  TUV: "#5892B5",
  UGA: "#3800A8",
  UKR: "#3826BD",
  ARE: "AE",
  GBR: "GB",
  USA: "#54377B",
  UMI: "#009944",
  URY: "#3800A8",
  UZB: "#1C90C5",
  VUT: "#D7000B",
  VEN: "#FADC00",
  VNM: "#F90000",
  ESH: "EH",
  YEM: "#EB0000",
  ZMB: "#00A95F",
  ZWE: "#F6FD00"
}
class CountryOrigin extends React.Component {
  constructor(props) {
    super(props);
    this.offset = 0;
    this.state = {
      countryOriginList: [],
      isloading: true,
      page: 1,
      isRefresh: false,
      scrollOffset: new Animated.Value(0),
      titleWidth: 0,
    }
  }
  _funDetails(id, prefix, exchangehousename, nooftransaction, amount, currency, color, screeId) {
    this.props.navigation.navigate("Details", {
      exID: id,
      exPrefix: prefix,
      exExHouseName: exchangehousename,
      exNoTrns: nooftransaction,
      exAmount: amount,
      exCurrency: currency,
      excolor: color,
      exScreenId: screeId
    });
  }

  handleCountryOfOriginList(userObj) {
    userObj = JSON.parse(userObj);
    var startDate = new Date
    var endDate = new Date
    //1 / 10 / 2018
    //startDate.toLocaleDateString('en-GB')
    //endDate.toLocaleDateString('en-GB')
    //alert(JSON.stringify(userObj.Accesstoken) + startDate + endDate)
    //alert(this.state.page)
    getCountryOriginList(this.state.page, "10/10/2018", "10/20/2018", userObj.Accesstoken).then((res) => {
      //alert(JSON.stringify(res))
      if (res === "Access denied") {
        this.props.navigation.navigate("Login");
        Toast.show("Access denied. Session expired, please login again.", Toast.SHORT);
      } else {
        if (this.state.page === 1) {
          this.setState({
            countryOriginList: res,
            isloading: false,
            isRefresh: false
          })
        }
        else {
          this.setState({
            countryOriginList: [...this.state.countryOriginList, ...res],
            isloading: false,
            isRefresh: false
          })
        }
      }
    }).
      catch(error => {
        alert("Please check your internet connection & try again.");  //error        
      });
  }
  //onscroll
  onScroll = e => {
    const scrollSensitivity = 4 / 3;
    const offset = e.nativeEvent.contentOffset.y / scrollSensitivity;
    this.state.scrollOffset.setValue(offset);
  };
  //call the api and recods from server
  componentDidMount() {
    //alert("dfdfdfd")
    try {
      AsyncStorage.getItem("userObj")
        .then(value => {
          this.handleCountryOfOriginList(value)
        }).done();
    }
    catch (error) {

    }
    this.state.scrollOffset.addListener(({ value }) => (this.offset = value));
  }
  //list will be updated on pull to refresh
  onRefresh() {
    try {
      AsyncStorage.getItem("userObj")
        .then(value => {
          this.setState({ isRefresh: true, page: 1 })
          this.handleCountryOfOriginList(value)
        }).done();
    }
    catch (error) {

    }
    //this.setState({ isRefresh: true, page: 1 }, function () { this.handleExchangeHouseList() });
  }

  _cardSwiped(item) {

  }
  handleloadMore() {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        //console.log("page2", this.state.page)
        try {
          AsyncStorage.getItem("userObj")
            .then(value => {
              //this.setState({ isRefresh: true, page: 1 })
              this.handleCountryOfOriginList(value)
            }).done();
        }
        catch (error) {

        }
        // this.handleExchangeHouseList();
      }
    );
  };

  renderItem = ({ item, index }) => {
    return (
      <View style={styles.container}>
        <Swiper
          cards={['DO', 'MORE', 'OF', 'WHAT', 'MAKES', 'YOU', 'HAPPY']}
          renderCard={(card) => {
            return (
              <View style={styles.card}>
                <Text style={styles.text}>{card}</Text>
              </View>
            )
          }}
          onSwiped={(cardIndex) => { console.log(cardIndex) }}
          onSwipedAll={() => { console.log('onSwipedAll') }}
          cardIndex={0}
          backgroundColor={'#4FD0E9'}
          stackSize={3}>
          <Button
            onPress={() => { console.log('oulala') }}
            title="Press me">
            You can press me
            </Button>
        </Swiper>
      </View>
      // <View
      //   style={{
      //     backgroundColor:
      //       index % 2 === 0
      //         ? AppColor.LIST_EVEN_BKCOLOR
      //         : AppColor.LIST_ODD_BKCOLOR
      //   }}
      // >
      //   <TouchableOpacity
      //     onPress={() =>
      //       this._funDetails(
      //         item.Id,
      //         item.ISOAlpha3.length == 1 ? "" : countryISOMapping[item.ISOAlpha3],
      //         item.CountryName,
      //         item.TRAN_NO,
      //         item.AMOUNT,
      //         item.CURRENCY === null ? "" : item.CURRENCY,
      //         countryColorMapping[item.ISOAlpha3],
      //         "1"
      //       )
      //     }
      //   >
      //     <View
      //       style={{
      //         marginLeft: 10,
      //         marginRight: 10,
      //         flex: 1,
      //         height: 77,
      //         flexDirection: "row",
      //         justifyContent: "center",
      //         alignContent: "center",
      //         alignItems: "center"
      //       }}
      //     >
      //       <View>
      //         <Image
      //           source={{
      //             uri: 'https://www.countryflags.io/' + countryISOMapping[item.ISOAlpha3] + '/flat/64.png'
      //           }}
      //           style={{ width: 50, height: 50, borderRadius: 0 }}
      //         />
      //       </View>

      //       <View
      //         style={{
      //           flex: 1,
      //           marginLeft: 10,
      //           flexDirection: "column",
      //           justifyContent: "flex-start",
      //           alignItems: "flex-start",
      //           alignContent: "space-around"
      //         }}
      //       >
      //         <View>
      //           <Text
      //             style={{
      //               fontFamily: "Roboto-Medium",
      //               fontSize: 14,
      //               paddingBottom: 1,
      //               color: "#141416"
      //             }}
      //           >
      //             {item.CountryName}
      //           </Text>
      //           <Text
      //             style={{
      //               fontFamily: "Roboto-Regular",
      //               fontSize: 12,
      //               color: "#9d9d9d"
      //             }}
      //           >
      //             No of Transactions:{item.TRAN_NO}
      //           </Text>
      //         </View>
      //       </View>
      //       <View style={{ marginVertical: 10 }}>
      //         <Text
      //           style={{
      //             color: AppColor.LIST_LEFT_TITLE,
      //             fontFamily: "Roboto-Bold",
      //             fontSize: 14,
      //             color: "#141416"
      //           }}
      //         >
      //           {getNumberWithCommas(item.AMOUNT) + " " + (item.CURRENCY === null ? "" : item.CURRENCY)}
      //         </Text>
      //       </View>
      //     </View>
      //   </TouchableOpacity>
      // </View>
    );
  };
  render() {
    console.log("EXH", this.state.isloading);
    const { scrollOffset } = this.state;
    const screenWidth = Dimensions.get('window').width;
    if (this.state.isloading) {
      return (
        <AppBar />
      );
    }
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#ffffff",
        }}
      >
        <CommonHeader navigation={this.props.navigation} />
        <Animated.View
          style={[
            styles.header,
            {
              backgroundColor: "#ffffff",
              paddingHorizontal: screenWidth * 0.05,
              width: screenWidth,
              height: scrollOffset.interpolate({
                inputRange: [0, 160],
                outputRange: [80, 37],
                extrapolate: 'clamp',
              }),
            },
          ]}>
          <Animated.Text
            onLayout={e => {
              if (this.offset === 0 && this.state.titleWidth === 0) {
                const titleWidth = e.nativeEvent.layout.width;
                this.setState({ titleWidth });
              }
            }}
            style={{
              color: "#1b1c1e",
              fontSize: 27,
              paddingBottom: 7,
              fontFamily: "Roboto-Black",
              fontSize: scrollOffset.interpolate({
                inputRange: [0, 200],
                outputRange: [26, 20],
                extrapolate: 'clamp',
              }),
            }}>
            {ScreenName.COUNTRY_ORIGIN}
          </Animated.Text>
          <View style={{ position: 'absolute', right: 10 }}>
            <Text style={{
              color: "#9d9d9d",
              fontSize: 15,
              paddingBottom: 20,
              fontFamily: "Roboto-Medium"
            }}>
              Today's
          </Text>
          </View>
          <Animated.View
            style={{
              width: scrollOffset.interpolate({
                inputRange: [0, 100],
                outputRange: [screenWidth * 0.9 - this.state.titleWidth, 0],
                extrapolate: 'clamp',
              }),
            }}
          />
        </Animated.View>
        <View
          style={{
            borderBottomColor: "#e7e7e7",
            borderBottomWidth: 0.4,
            marginLeft: 60
          }}
        />
        <View style={styles.container}>
          <Swiper
            ref={swiper => {
              this.swiper = swiper;
            }}
            verticalSwipe={false}
            cards={['Exchange House', 'BBL Branch', 'Country of Origin', 'Transaction Mode', 'Exchange House', 'BBL Branch', 'Country of Origin']}
            renderCard={(card) => {
              return (
                <View
                  style={{
                    flex: 0.60,
                    margin: 10,
                    backgroundColor: AppColor.THEME_COLOR,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 10
                  }}
                >
                  <Text style={{ fontSize: 36, color: "#fff", fontFamily: "Roboto-Bold" }}>34,33,434$</Text>
                  <Text style={{ fontSize: 22, color: "#fff", fontFamily: "Roboto-Light" }}>Total Transactions</Text>
                  <Text style={{ fontSize: 18, color: "#fff", fontFamily: "Roboto-Light" }}>{card}</Text>
                </View>
              )
            }}
            onSwipedAll={this.onSwipedAllCards}
            swipeAnimationDuration={350}
            cardIndex={0}
            onSwiped={item => this._cardSwiped(item)}
            infinite={true}
            stackSize={20}
            verticalThreshold={50}
            disableTopSwipe={true}
            disableBottomSwipe={true}
            stackSeparation={1}
            backgroundColor={"transparent"}
            animateOverlayLabelsOpacity
            animateCardOpacity
          />
        </View>
        {/* <FlatList
          style={{ width: '100%', flexGrow: 1, marginBottom: 0 }}//90
          data={this.state.countryOriginList}
          onScroll={this.onScroll}
          renderItem={(item, index) => this.renderItem(item, index)}
          keyExtractor={item => item.Id}
          onRefresh={() => this.onRefresh()}
          refreshing={this.state.isRefresh}
          onEndReached={({ distanceFromEnd }) => {
            this.handleloadMore();
          }}
          onEndReachedThreshold={0.5}
        /> */}
      </View>
    );
  }
}

onSwipedAllCards = () => {
};
swipeLeft = () => {
  this.swiper.swipeLeft();
};
export default CountryOrigin;
const styles = StyleSheet.create({
  header: {
    backgroundColor: 'whitesmoke',
    borderBottomWidth: 1,
    borderColor: 'gainsboro',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingBottom: 8,
  },
  container: {
    flex: 3,
    backgroundColor: "#F5FCFF"
  },
  card: {
    flex: 1,
    borderRadius: 4,
    height: 60,
    borderWidth: 2,
    borderColor: "#412A26",
    justifyContent: "center",
    backgroundColor: "#694e4a"
  },
  text: {
    textAlign: "center",
    fontSize: 50,
    backgroundColor: "transparent"
  }
});
