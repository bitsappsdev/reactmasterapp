/**
 * Created By       : Md. Golam Sarwar
 * Date             : 10 Dec 2018
 * Purpose          : TAB Bar Chart Scenes for Pie chart example
 */

import React, { Component } from 'react'
import { AsyncStorage } from "react-native"
import { getAmountVsDaysList } from "../../api/FetchBarCharAmountVsDays";
import AppBar from "../../components/AppBars";
import PieChart from '../../components/PieChart';

export default class PieChartExample extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amountVsDaysList: [],
            isloading: true,
        }
    }

    handleAmountVsDaysList(userObj) {
        userObj = JSON.parse(userObj);

        getAmountVsDaysList(1, "10/10/2018", "10/20/2018", userObj.Accesstoken).then((res) => {
          if (res === "Access denied") {
            this.props.navigation.navigate("Login");
            Toast.show("Access denied. Session expired, please login again.", Toast.SHORT);
          } else {
            this.setState({
              amountVsDaysList: res,
              isloading: false,
            })
          }
        })
    }    

    componentDidMount() { 
        this.props.navigation.addListener('willFocus', this.load)
    }
    

    load = () => {    
        this.setState({
          isloading: true
        });    
        try {
          AsyncStorage.getItem("userObj")
            .then(value => {
              this.handleAmountVsDaysList(value)
            }).done();
        }
        catch (error) {    
        }
      }

  render() {
    if (this.state.isloading) {
      return (
        <AppBar />
      );
    }

    return (
      <PieChart dataList={this.state.amountVsDaysList}/>
    )
  }
}
