/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Date             : 19 Jul 2018
 * Purpose          : TAB Exchange House Scenes
 */
import React from "react";
import { FlatList, Text, TouchableOpacity, View,ToastAndroid } from "react-native";
import { getExchangeHouseList } from "../api/FetchExchangeHouse";
import { getNumberWithCommas } from "../api/Helpers";
import AppBar from "../components/AppBars";
import * as AppColor from "../styles/Color";
import * as ScreenName from "../../src/navigation/screenames";

var randomColor = require("randomcolor"); // import the script
class FilteredList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      exchangeHouseList: [],
      isloading: true,
      page: 1,
      isRefresh: false
    }
  }
  _funDetails(Value) {
    ToastAndroid.show('Filtered item - ' + Value, ToastAndroid.SHORT);
    // this.props.navigation.navigate("Details", {
    //   exID: id,
    //   exPrefix: prefix,
    //   exExHouseName: exchangehousename,
    //   exNoTrns: nooftransaction,
    //   exAmount: amount
    // });
  }

  handleExchangeHouseList() {
    getExchangeHouseList(this.state.page).then((res) => {
      if (this.state.page === 1) {
        this.setState({
          exchangeHouseList: res.data,
          isloading: false,
          isRefresh: false
        })
      }
      else {
        this.setState({
          exchangeHouseList: [...this.state.exchangeHouseList, ...res.data],
          isloading: false,
          isRefresh: false
        })
      }
    })
  }

  //call the api and recods from server
  componentDidMount() {
    this.handleExchangeHouseList();
  }
  //list will be updated on pull to refresh
  onRefresh() {
    this.setState({ isRefresh: true, page: 1 }, function () { this.handleExchangeHouseList() });
  }

  handleloadMore() {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        console.log("page2", this.state.page)
        this.handleExchangeHouseList();
      }
    );
  };

  renderItem = ({ item }) => {
    return (
      <View
        style={{
          backgroundColor:
            item.id % 2 == 0
              ? AppColor.LIST_EVEN_BKCOLOR
              : AppColor.LIST_ODD_BKCOLOR
        }}
      >
        <TouchableOpacity
          onPress={() =>this._funDetails(item.exchangehousename)}>
          <View
            style={{
              marginLeft: 10,
              marginRight: 10,
              flex: 1,
              height: 77,
              flexDirection: "row",
              justifyContent: "center",
              alignContent: "center",
              alignItems: "center"
            }}
          >
            <View
              style={{
                backgroundColor: randomColor(),
                width: 40,
                height: 40,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 40
              }}
            >
              <Text
                style={{
                  color: "#fff",
                  fontFamily: "Roboto-Bold",
                  fontSize: 16
                }}
              >
                {item.prefix}
              </Text>
            </View>

            <View
              style={{
                flex: 1,
                marginLeft: 10,
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "flex-start",
                alignContent: "space-around"
              }}
            >
              <View>
                <Text
                  style={{
                    fontFamily: "Roboto-Medium",
                    fontSize: 14,
                    paddingBottom: 1,
                    color: "#141416"
                  }}
                >
                  {item.exchangehousename}
                </Text>
                <Text
                  style={{
                    fontFamily: "Roboto-Regular",
                    fontSize: 12,
                    color: "#9d9d9d"
                  }}
                >
                  No of Transactions:{item.nooftransaction}
                </Text>
              </View>
            </View>
            <View style={{ marginVertical: 10 }}>
              <Text
                style={{
                  color: AppColor.LIST_LEFT_TITLE,
                  fontFamily: "Roboto-Bold",
                  fontSize: 14,
                  color: "#141416"
                }}
              >
                {getNumberWithCommas(item.amount) + " " + item.currency}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    if (this.state.isloading) {
      return (
        <AppBar />
      );
    }
    return (
      <View
        style={{
          backgroundColor: "#ffffff"
        }}
      >
        <View
          style={{
            alignItems: "flex-start",
            margin: 10
          }}
        >
          <Text
            style={{
              color: "#1b1c1e",
              fontSize: 27,
              paddingBottom: 7,
              fontFamily: "Roboto-Black"
            }}
          >
            {ScreenName.EXCHANGE_HOUSE}
          </Text>
        </View>
        <View
          style={{
            borderBottomColor: "#e7e7e7",
            borderBottomWidth: 0.4,
            marginLeft: 60
          }}
        />
        <FlatList
          style={{ width: '100%', flexGrow: 1, marginBottom: 60 }}
          data={this.state.exchangeHouseList}
          renderItem={item => this.renderItem(item)}
          keyExtractor={item => item.id}
          onRefresh={() => this.onRefresh()}
          refreshing={this.state.isRefresh}
          onEndReached={({ distanceFromEnd }) => {
            this.handleloadMore();
          }}
          onEndReachedThreshold={0.5}
        />
      </View>
    );
  }
}
export default FilteredList;
