/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Date         : 16 July 2018
 * Purpose      : Details + Tab layout
 */
import Dimensions from "Dimensions";
import React from "react";
import { Alert, AsyncStorage, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Dialog } from "react-native-simple-dialogs";
import RadioForm from "react-native-simple-radio-button";
import Slider from "react-native-slider";
import AppStatusBar from "../components/AppStatusBar";
import OfflilneNotice from "../components/OfflineNotice";
import RemitTab from "../navigation/navigations";
import UserProfile from "../scenes/UserProfile"
import CommonHeader from "../components/CommonHeader"
import RangeSlider from "react-native-range-slider";
import * as AppColor from "../styles/Color";
import * as AppsString from "../styles/ConstantString";
import AppContainer from "../styles/Container";
var { width, height } = Dimensions.get("window");
const items = [
  1337,
  "janeway",
  {
    lots: "of",
    different: {
      types: 0,
      data: false,
      that: {
        can: {
          be: {
            quite: {
              complex: {
                hidden: ["gold!"]
              }
            }
          }
        }
      }
    }
  },
  [4, 2, "tree"]
];
var radio_props_sort_by = [
  { label: "Ascending", value: 0 },
  { label: "Descending", value: 1 }
];
var radio_props_duration_by = [
  { label: "Daily", value: 0 },
  { label: "Weekly", value: 1 },
  { label: "Monthly", value: 2 }
];
class Detail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogFilterVisible: false,
      dialogVisible: false,
      //sort by radio form
      initial_sort_item: 0,
      value: 10,
      //Duration
      initial_duration_item: 0,
      //
      isToggle: false,
      //detail header show off
      screenID: 0,
      //Mode selected
      isDCSelected: false,
      isEFTSelected: false,
      isbKashSelected: false,
      isSpotCashSelected: false
    };
  }
  _handleResults(results) {
    // this.setState({ results: 1 });
    alert("SEARCH");
  }
  static navigationOptions = {
    header: null
  };
  funUserLogout = () => {
    this.props.navigation.navigate("Login");
  };
  //Apply
  funApply() {
    this.setState({ dialogFilterVisible: false })
    this.props.navigation.navigate("FilteredList")
  };
  //Cancel
  funCancel = () => {
    this.setState({ dialogFilterVisible: false });
  };
  //sort_by_select
  sort_by_select = item => {
    this.setState({
      initial_sort_item: item.value
    });
  };
  //Duration
  duration_by_select = item => {
    this.setState({
      initial_duration_item: item.value
    });
  };
  // @action: logouts user
  funSignout = () => {
    Alert.alert("Sign out", "Signing out your account?", [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      { text: "OK", onPress: this.funUserLogout }
    ]);
  };
  funUserProfile = () => {
    this.props.navigation.navigate("UserProfile");
    // NavigationActions.dispatch({ type: 'NAVIGATE', path: "UserProfile" });
  };
  openDialog(show) {
    //  alert("Out side click")
    this.setState({ dialogFilterVisible: show });
  }
  funFilter = () => {
    this.setState({
      dialogFilterVisible: true
    });
    console.log("Filter");
  };
  funSearch = () => {
    this.props.navigation.navigate("Search")
  };
  funModeChoose = (id) => {
    switch (id) {
      case 0:     //Direct Credit
        {
          this.state.isDCSelected == false ? this.setState({
            isDCSelected: true
          }) : this.setState({
            isDCSelected: false
          })
        }
        break;
      case 1:     //EFT
        this.state.isEFTSelected == false ? this.setState({
          isEFTSelected: true
        }) : this.setState({
          isEFTSelected: false
        })
        break;
      case 2:     //SpotCash
        this.state.isSpotCashSelected == false ? this.setState({
          isSpotCashSelected: true
        }) : this.setState({
          isSpotCashSelected: false
        })
        break;
      case 3:     //bKash
        this.state.isbKashSelected == false ? this.setState({
          isbKashSelected: true
        }) : this.setState({
          isbKashSelected: false
        })
        break;
      default:
        break;

    }

  };
  // componentDidMount() {


  //   AsyncStorage.getItem("ScreenID").then(item => {
  //     if (item === 1) {
  //       this.setState({
  //         screenID: 0
  //       });
  //     }
  //   }); //Screen ID 1 mean it is Master
  // }
  render() {

    return (
      <View style={styles.container}>
        <AppStatusBar />
        <OfflilneNotice />
        <View style={{ marginTop: 0 }} />
        {/* <RemitTab /> */}
      </View>
    );
  }
}
export default Detail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "stretch",
    width: null,
    paddingTop: 0,
    backgroundColor: "#ffffff"
  },
  modeClick: {
    backgroundColor: "red"
  },
  border1: {
    borderColor: "#979797",
    borderWidth: 0.5,
    height: 50,
    width: 130,
    justifyContent: "space-around",
    //flexDirection: "row",
    alignContent: "center",
    alignItems: "center",
    margin: 5
  },
  border2: {
    borderColor: "#979797",
    borderWidth: 0.5,
    height: 50,
    backgroundColor: "#2ecc71",
    width: 130,
    justifyContent: "space-around",
    //flexDirection: "row",
    alignContent: "center",
    alignItems: "center",
    margin: 5
  },
  textColorApply: {
    color: "#ffffff"
  },
  textColorCancel: {
    color: "#1b1c1e"
  }
});
