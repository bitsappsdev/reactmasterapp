/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Purpose          : Constant String Entire Apps
 * Date             : 12 Jul 2018
 */
import Dimensions from "Dimensions";
export const BASE_URL = "http://10.100.59.169:5000/api";//office 
//export const BASE_URL_DEV = "http://10.5.2.238/masterappAppAPI/api";//Development Server
//export const BASE_URL = "http://192.168.1.32:5000/api";//home 
//export const BASE_URL = "http://103.3.63.67:5001/api";
export const BASE_URL_DEV = "http://118.179.217.238/mRemitAppAPI/api";//Development Server
export const LOGIN_TO_YOUR_ACCOUNT = "Login to your account";
export const MAX_Length_USER_NAME = 31;
export const MAX_Length_USER_PASSWORD = 31;
export const PLACEHOLDER_USER_NAME = "User name or email";
export const PLACEHOLDER_USER_PASSWORD = "Password";
export const SUBMIT_BUTTON_TEXT = "LOGIN";
export const DEVICE_WIDTH = Dimensions.get("window").width;
export const DEVICE_HEIGHT = Dimensions.get("window").height;
//----Transaction MOde ----//
export const TRANSACTION_MODE = "Transaction Mode";
export const DIRECT_CREDIT = "Direct Credit";
export const EFT = "EFT";
export const SOPT_CASH = "Spot Cash";
export const BKASH = "bKash";
export const SORT_BY = "Sort by";
export const ASCENDING = "Ascending";
export const DECENDING = "Decending";
export const AMOUNT_RANG = "Amount Rang";
export const AMOUNT = "10k-100k";
export const DURATION = "Duration";
export const DAILTY = "Daily";
export const MONTHLY = "Monthly";
export const WEEKLY = "Weekly";
export const BTN_CANCEL = "Cancel";
export const BTN_APPLY = "Apply";
export const PER_PAGE = 15;