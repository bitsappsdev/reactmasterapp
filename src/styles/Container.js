/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Date         : 12 Jul 2018
 * Pupose       : Common Container for entire apps
 */
import { StyleSheet } from "react-native";
import * as CustomColor from "./Color";
import { Platform } from "react-native";
const container = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: "center",
    marginTop: Platform.OS == "ios" ? 0 : 0,
    backgroundColor: "#ffffff"
  },
  Background: {
    flex: 1,
    justifyContent: "center",
    width: null,
    height: null
  },
  Center: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "stretch",
    alignItems: "center",
  },
  //Start :  LOGIN
  LoginLogoContainer: {
    marginTop: 35.3,
    marginLeft: 23.3,
    flex: 2
  },
  LoginLogo: {
    width: 159.7,
    height: 42.7
  },
  LoginText: {
    color: CustomColor.LOGIN_TEXT_COLOR, //"#8a8c91",
    fontWeight: "normal",
    fontSize: 19.3,
    backgroundColor: CustomColor.TRANSPARTENT_COLOR,
    marginTop: 5,
    fontFamily: "Roboto-Light"
  },
  //END : LOGIN
  DetailsHeader: {
    backgroundColor: CustomColor.THEME_COLOR,
    flex: 1.7
  },
  DetailsHeaderContent: {
    justifyContent: "flex-start",
    alignItems: "center",
    alignContent: "center"
  },
  DetailsList: {
    flex: 3.3,
    margin: 0
  },
  DetailsHeaderText1: {
    color: CustomColor.SUBMIT_TEXT_COLOR,
    fontSize: 15,
    fontFamily: "Roboto-Medium"
  },
  DetailsHeaderText2: {
    color: CustomColor.SUBMIT_TEXT_COLOR,
    fontFamily: "Roboto-Bold",
    fontSize: 22
  },
  //Screen Nmae : Detail
  //Purpose  : Filter
  DetailTransactionModeView: {
    borderRadius: 30,
    height: 30,
    margin: 2,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    borderColor: CustomColor.THEME_COLOR,
    borderWidth: 0.7,
  },
  DetailTransactionModeViewSelected: {
    backgroundColor: CustomColor.THEME_COLOR,
    borderRadius: 30,
    height: 30,
    margin: 2,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    borderColor: CustomColor.THEME_COLOR
  },
  DetailsTransactionModeText: {
    color: CustomColor.FILTER_RADIO_BTN_TEXT_COLOR,
    fontSize: 12,
    padding: 7,
    fontFamily: "Roboto-Regular"
  },
  DetailsTransactionModeTextSelected: {
    color: "#ffffff",
    fontSize: 12,
    padding: 7,
    fontFamily: "Roboto-Regular"
  },
  FilterButtonText: {
    color: CustomColor.FILTER_BUTTON_TEXT,
    fontWeight: "500",
    fontSize: 13,
    fontFamily: "Roboto-Medium"
  },
  //animated header for all list title
  AnimatedHeader: {
    backgroundColor: 'whitesmoke',
    borderBottomWidth: 1,
    borderColor: 'gainsboro',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingBottom: 8,
  }
});
export default container;
