/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Purpose          : Constant Color Code Entire Apps
 * Date             : 12 Jul 2018
 */
export const LOGIN_TEXT_COLOR = "#8a8c91";
export const TRANSPARTENT_COLOR = "transparent";
export const THEME_COLOR = "#412A26";
export const STATUS_BAR_COLOR = "#220000";
export const PLACRHOLDER_TEXT_COLOR = "#bbbcc0";
export const TEXT_INPUT_COLOR = "#000000";
export const SUBMIT_TEXT_COLOR = "#ffffff";
export const ERROR = "red";
export const FILTER_BUTTON_TEXT = "#1b1c1e";
export const SILDE_TINT_COLOR = "#d8d8d8";
//LIST
export const LIST_FONT_COLOR_TITLE = "#141416";
export const LIST_FONT_COLOR_SUB_TITLE = "#9d9d9d";
export const LIST_LEFT_TITLE = "#27272a";
export const LIST_ODD_BKCOLOR = "#f5f6f7";
export const LIST_EVEN_BKCOLOR = "#ffffff";
//Filter
export const FILTER_RADIO_BTN_COLOR = "#694e4a";
export const FILTER_BUTTON_DEFULT = "#f5f6f7"
export const FILTER_RADIO_BTN_TEXT_COLOR = "#1b1c1e";

//TAB BAR
export const TAB_BAR_SELECTED_TINT_COLOR = "#6A4E4A";
export const TAB_BAR_UNSELECTED_TINT_COLOR = "#1b1c1e";
export const TAB_BAR_TINT = "#e7e7e7";

export const HEADER_COLOR = "#402A25"
