/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Created Date : 12 Jul 2018
 * Puporse      : Common API Station
 */
import * as ConstantString from "../styles/ConstantString";
//------------GET AMOUNT VS DAYS LIST--------
export const getAmountVsDaysList = (Offset, StartDate, EndDate, token) => {
  //   const URL = ConstantString.BASE_URL + "/barchartamountvsdays/";
  //   return fetch(URL).then((res) => res.json());
  // };
  const URL = ConstantString.BASE_URL_DEV + "/TranMode/Chart/";
  return fetch(
    URL,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        TotalRecord: 7,//ConstantString.PER_PAGE,
        Offset: Offset,
        StartDate: StartDate,
        EndDate: EndDate,
        Token: token
      })
    }
  )
    .then(response => response.json())
};