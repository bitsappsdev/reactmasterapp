/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Created Date : 12 Jul 2018
 * Puporse      : Common API Station
 */
import * as ConstantString from "../styles/ConstantString";
//------------GET TRANSACTION MODE LIST--------
export const getTransactionModeList = (Offset, StartDate, EndDate, token) => {
  const URL = ConstantString.BASE_URL_DEV + "/TranMode/AllTranMode/";
  //alert(Offset)
  return fetch(
    URL,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        TotalRecord: "15",
        Offset: Offset,
        StartDate: StartDate,
        EndDate: EndDate,
        Token: token
      })
    }
  )
    .then(response => response.json())
};
