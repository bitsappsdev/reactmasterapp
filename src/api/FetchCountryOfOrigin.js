/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Created Date : 12 Jul 2018
 * Puporse      : Common API Station
 */
import * as ConstantString from "../styles/ConstantString";
//------------GET COUNTRY OF ORIGIN LIST--------
export const getCountryOriginList = (Offset, StartDate, EndDate, token) => {
  const URL = ConstantString.BASE_URL_DEV + "/Country/CntSummary/";
  return fetch(
    URL,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        TotalRecord: ConstantString.PER_PAGE,
        Offset: Offset,
        StartDate: StartDate,
        EndDate: EndDate,
        Token: token
      })
    }
  )
    .then(response => response.json())
};
//-----------GET COUNTRY DETAILS------
export const getCountryOriginDetails = (exhouseId, StartDate, EndDate, token) => {
  //alert(exhouseId)
  const URL = ConstantString.BASE_URL_DEV + "/Country/CntDetails/";
  return fetch(
    URL,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        TotalRecord: ConstantString.PER_PAGE,
        Offset: 1,
        StartDate: StartDate,
        EndDate: EndDate,
        Token: token,
        CountryId: exhouseId //country id
      })
    }
  )
    .then(response => response.json())
};
