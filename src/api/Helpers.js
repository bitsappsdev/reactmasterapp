/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Created Date : 12 Jul 2018
 * Puporse      : Common API Station
 */
export function getNumberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};