/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Created Date : 12 Jul 2018
 * Puporse      : Common API Station
 */
import * as ConstantString from "../styles/ConstantString";
//------------GET USER INFO--------
export const getUserInfo = name => {
  let username = name.toLowerCase().trim();
  const URL = ConstantString.BASE_URL + "/users/${username}";
  return fetch(URL).then(res => res.json());
};
