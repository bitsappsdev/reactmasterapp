/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Created Date : 12 Jul 2018
 * Puporse      : Common API Station
 */
import * as ConstantString from "../styles/ConstantString";
//------------GET EXCHANGE HOUSE LIST--------
export const getExchangeHouseList = (Offset, StartDate, EndDate, token) => {
  const URL = ConstantString.BASE_URL_DEV + "/ExchangeHouse/ExHsummary/";
  return fetch(
    URL,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        TotalRecord: ConstantString.PER_PAGE,
        Offset: Offset,
        StartDate: StartDate,
        EndDate: EndDate,
        token: token
      })
    }
  )
    .then(response => response.json())
};
//-----------GET EXCHANGE HOUSE DETILS------
export const getExchangeHouseDetails = (exhouseId, StartDate, EndDate, token) => {
  const URL = ConstantString.BASE_URL_DEV + "/ExchangeHouse/ExHdetails/";
  return fetch(
    URL,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        ExhId: exhouseId,
        StartDate: StartDate,
        EndDate: EndDate,
        token: token
      })
    }
  )
    .then(response => response.json())
};
// GET EX HOUSE FROM BRANCH WISE
export const getBranchWiseExchangeHouseList = (Offset, StartDate, EndDate, token, BranchId) => {
  const URL = ConstantString.BASE_URL_DEV + "/Branch/BrnchWiseExh/";
  return fetch(
    URL,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        TotalRecord: ConstantString.PER_PAGE,
        Offset: Offset,
        StartDate: StartDate,
        EndDate: EndDate,
        Token: token,
        BranchId: BranchId
      })
    }
  )
    .then(response => response.json())
};