/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Created Date : 12 Jul 2018
 * Puporse      : Common API Station
 */
import * as ConstantString from "../styles/ConstantString";
//------------GET BBL BRANCH LIST--------
export const getBBLBranchList = (Offset, StartDate, EndDate, token) => {
  const URL = ConstantString.BASE_URL_DEV + "/Branch/AllBranch/";
  return fetch(
    URL,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        TotalRecord: ConstantString.PER_PAGE,
        Offset: Offset,
        StartDate: StartDate,
        EndDate: EndDate,
        Token: token
      })
    }
  )
    .then(response => response.json())
};
//-----------GET EXCHANGE HOUSE DETILS------
export const getBBLBranchDetails = (exhouseId, StartDate, EndDate, token) => {
  const URL = ConstantString.BASE_URL_DEV + "/Branch/AllBranch/";
  return fetch(
    URL,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        ExhId: exhouseId,
        StartDate: StartDate,
        EndDate: EndDate,
        token: token
      })
    }
  )
    .then(response => response.json())
};
