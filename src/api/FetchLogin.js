/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Created Date : 12 Jul 2018
 * Puporse      : Common API Station
 */
import * as ConstantString from "../styles/ConstantString";
//------------GET USER AUTHENTICATION INFO--------
export const getLogin = (username, password) => {
  //alert(username + password)
  //const URL = ConstantString.BASE_URL_DEV + "/token?UserName=" + username + "&Password=" + password;
  //return fetch(URL).then((res) => res.json());
  const URL = ConstantString.BASE_URL_DEV + "/Token";
  return fetch(
    URL,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        UserName: username,
        Password: password,
      })
    }
  )
    .then(response => response.json())
};
