import React from "react";
import { TabNavigator } from "react-navigation";
import { Text, Platform } from "react-native";
//import your tabs js file
import AmountVsDays from "../scenes/tabs/AmountVsDays";
import TransNoVsDays from "../scenes/tabs/TransNoVsDays";
import PieChartExample from "../scenes/tabs/PieChartExample";

import * as AppColor from "../styles/Color";
const Tab = TabNavigator(
  {
    // here you will define your screen-tabs
    "Amount vs Days": {
      screen: AmountVsDays
      // navigationOptions: {
      //   headerTitle: <Text>{"Amount" + " vs".toLowerCase() + " Days"}</Text>,
      //   title: <Text>{"Amount" + " vs".toLowerCase() + " Days"}</Text>,
      //   showLabel: false
      // }
    },
    "Trans No. vs Days": {
      screen: TransNoVsDays
      // navigationOptions: {
      //   showLabel: false,
      //   tabBarLabel: <Text>{"Trans No" + " vs".toLowerCase() + " Days"}</Text>
      // }
    }
    ,
    "Pie Chart Example": {
      screen: PieChartExample
      // navigationOptions: {
      //   showLabel: false,
      //   tabBarLabel: <Text>{"Trans No" + " vs".toLowerCase() + " Days"}</Text>
      // }
    }
  },
  {
    tabBarPosition: "top",
    swipeEnabled: true,
    initialRouteName: "Amount vs Days",
    tabBarOptions: {
      // pressColor: AppColor.TAB_BAR_SELECTED_TINT_COLOR,
      activeBackgroundColor: "#f5f6f7",
      // inactiveBackgroundColor: "#EFEFEF",
      upperCaseLabel: false,
      activeTintColor: AppColor.TAB_BAR_SELECTED_TINT_COLOR,
      inactiveTintColor: AppColor.TAB_BAR_UNSELECTED_TINT_COLOR,
      labelStyle: {
        fontSize: 14,
        padding: 0,
        //fontFamily: "Roboto-Medium"
      },
      indicatorStyle: {
        backgroundColor: AppColor.TAB_BAR_SELECTED_TINT_COLOR
      },
      style: {
        //borderBottomWidth: 1,
        //borderBottomColor: "#323950",
        backgroundColor: "#ffffff" // Makes Android tab bar white instead of standard blue
      }
    }
  }
);
export default Tab;
// export default (Tab = StackNavigator({
//   /* other screens */
//   Tabs: {
//     screen: Tab // TabNavigator
//   },

//   // PollDetails: { screen: PollDetails },
//   // SuperPredict: { screen: SuperPredict }

//   /* other screens */
// }));
//export default Tab;
