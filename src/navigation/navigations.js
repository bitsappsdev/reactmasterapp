import React from "react";
import {
  StackNavigator,
  createBottomTabNavigator
} from "react-navigation";
import { Platform } from "react-native";
import { Image } from "react-native";
//import your tabs js file
import ExchangeHouse from "../scenes/tabs/ExchangeHouse";
import CountryOrigin from "../scenes/tabs/CountryOrigin";
import BBLBranch from "../scenes/tabs/BBLBranch";
import BBLBranchExHouse from "../scenes/tabs//BBLBranchExHouse.js"
import TransactionMode from "../scenes/tabs/TransactionMode";
import BarChart from "../scenes/tabs/BarChart";
import UserProfile from "../../src/scenes/UserProfile";
import Login from "../../src/scenes/Login"
import Details from "../scenes/Details";
import * as ScreenNames from "../../src/navigation/screenames";
import * as Color from "../styles/Color";
const Tab = createBottomTabNavigator(
  {
    // here you will define your screen-tabs
    ExchangeHouse: {
      screen: ExchangeHouse,
      navigationOptions: {
        showLabel: false,
        tabBarLabel: "Exng. House",
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require("../assets/icons/exchange_house.png")}
            style={{ tintColor: tintColor }}
          />
        ),
        showIcon: true
      }
    },
    CountryOrigin: {
      screen: CountryOrigin,
      navigationOptions: {
        showLabel: false,
        tabBarLabel: "Cout. of Origin",
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require("../assets/icons/origin_country.png")}
            style={{ tintColor: tintColor }}
          />
        ),
        showIcon: true
      }
    },
    BBLBranch: {
      screen: BBLBranch,
      navigationOptions: {
        showLabel: false,
        tabBarLabel: "BBL Branch",
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require("../assets/icons/bbl_branch.png")}
            style={{ tintColor: tintColor }}
          />
        ),
        showIcon: true
      }
    },
    TransactionMode: {
      screen: TransactionMode,
      navigationOptions: {
        showLabel: false,
        tabBarLabel: "Trans. Mode",
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require("../assets/icons/transaction_mode.png")}
            style={{ tintColor: tintColor }}
          />
        ),
        showIcon: true
      }
    },
    BarChart: {
      screen: BarChart,
      navigationOptions: {
        tabBarLabel: "Bar Chart",
        showLabel: false,
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require("../assets/icons/chart.png")}
            style={{ tintColor: tintColor }}
          />
        ),
        showIcon: true
      }
    }
  },

  {
    tabBarPosition: "bottom",
    swipeEnabled: true,
    initialRouteName: "ExchangeHouse",
    tabBarOptions: {
      activeTintColor: Color.TAB_BAR_SELECTED_TINT_COLOR,
      activeBackgroundColor: "#f5f6f7",
      inactiveTintColor: "#bbbbc1",
      labelStyle: {
        fontSize: 10,
        padding: 0
      },
      indicatorStyle: {
        backgroundColor: "green"
      },
      tabStyle: {
        paddingBottom: 5,
        paddingTop: 5
      },
      style: {
        marginTop: 3,
        height: 55,
        //marginBottom: 5,
        // borderBottomWidth: 1,
        // borderBottomColor: "#323950",
        backgroundColor: "#ffffff" // Makes Android tab bar white instead of standard blue
      }
    }
  }
);
//export default Tab;
export default (Tab = StackNavigator({
  /* other screens */
  Tabs: {
    screen: Tab,
    navigationOptions: {
      header: null
    } // TabNavigator
  },

  Details: {
    screen: Details,
    navigationOptions: {
      headerBackTitle: null, //here I mean that the title will not be shown on the NEXT(!) screen
      headerTitle: "Details",
      headerStyle: {
        backgroundColor: Color.HEADER_COLOR,
        elevation: null,
        shadowOpacity: 0,
        marginTop: 20
      },
      headerTitleStyle: {
        color: "#FFF",
        fontWeight: "500",
        fontSize: 18,
        textAlign: "center",
        flexGrow: 0.75,
        //alignSelf: Platform.OS === "android" ? "flex-end" : "center"
      },
      headerTintColor: "#fff"
    }
  },
  // Login: {
  //   screen: Login
  // },
  // UserProfile: {
  //   screen: UserProfile,
  //   navigationOptions: {
  //     headerBackTitle: null, //here I mean that the title will not be shown on the NEXT(!) screen
  //     headerTitle: "My Profile",
  //     headerStyle: {
  //       backgroundColor: "#02bd40",
  //       elevation: null,
  //       shadowOpacity: 0,
  //       marginTop: 0
  //     },
  //     headerTitleStyle: {
  //       color: "#FFF",
  //       fontWeight: "500",
  //       fontSize: 18,
  //       textAlign: "center",
  //       flexGrow: 0.75,
  //       //alignSelf: Platform.OS === "android" ? "flex-end" : "center"
  //     },
  //     headerTintColor: "#fff"
  //   }
  // },
  ExchangeHouseStack: {
    screen: BBLBranchExHouse,
    navigationOptions: {
      headerBackTitle: null, //here I mean that the title will not be shown on the NEXT(!) screen
      headerTitle: "Exchange Houses",
      headerStyle: {
        backgroundColor: Color.HEADER_COLOR,
        elevation: null,
        shadowOpacity: 0,
        marginTop: 20
      },
      headerTitleStyle: {
        color: "#FFF",
        fontWeight: "500",
        fontSize: 18,
        textAlign: "center",
        flexGrow: 0.75
        //alignSelf: Platform.OS === "android" ? "flex-end" : "center"
      },
      headerTintColor: "#fff"
    }
  }
  // SuperPredict: { screen: SuperPredict }

  /* other screens */
}));
//export default Tab;
