/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Purpose          : Screen Names
 * Date             : 12 Jul 2018
 */
export const SPLASH = "Splash";
export const LOGIN = "Login";
export const DETAIL = "Detail";
export const EXCHANGE_HOUSE = "Exchange House";
export const COUNTRY_ORIGIN = "Country of Origin";
export const BBL_BRANCE = "BBL Branch";
export const TRANSACTION_MODE = "Transaction Mode";
export const BAR_CHART = "Bar Chart";
export const DETAILS = "Details";
export const USER_PROFILE = "UserProfile";
export const FILTERED_LIST = "FilteredList";
export const SEARCHED ="Search"
export const CHANGE_PASSWORD ="ChangePassword"
export const MY_LOCATION = "MyLocation"
