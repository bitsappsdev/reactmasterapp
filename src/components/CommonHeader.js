/**
 * Created By   : Sree Bijoy Kumar Mohanta
 * Date         : 16 July 2018
 * Purpose      : Details + Tab layout
 */
import Dimensions from "Dimensions";
import React from "react";
import { Alert, AsyncStorage, Image, ScrollView, StyleSheet, Text, TouchableOpacity, TouchableHighlight, View } from "react-native";
import { Dialog } from "react-native-simple-dialogs";
import RadioForm from "react-native-simple-radio-button";
import Slider from "react-native-slider";
import RangeSlider from "react-native-range-slider";
import * as AppColor from "../styles/Color";
import * as AppsString from "../styles/ConstantString";
import AppContainer from "../styles/Container";
var { width, height } = Dimensions.get("window");
var radio_props_sort_by = [
    { label: "Ascending", value: 0 },
    { label: "Descending", value: 1 }
];
var radio_props_duration_by = [
    { label: "Daily", value: 0 },
    { label: "Weekly", value: 1 },
    { label: "Monthly", value: 2 }
];
class CommonHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dialogFilterVisible: false,
            dialogVisible: false,
            //sort by radio form
            initial_sort_item: 0,
            value: 10,
            //Duration
            initial_duration_item: 0,
            //
            isToggle: false,
            //detail header show off
            screenID: 0,
            //Mode selected
            isDCSelected: false,
            isEFTSelected: false,
            isbKashSelected: false,
            isSpotCashSelected: false
        };
        const nav = this.props.navigation;
    }
    _handleResults(results) {
        // this.setState({ results: 1 });
        alert("SEARCH");
    }
    static navigationOptions = {
        header: null
    };
    funUserLogout = () => {
        this.props.navigation.navigate("Login");
    };
    //Apply
    funApply() {
        this.setState({ dialogFilterVisible: false })
        this.props.navigation.navigate("FilteredList")
    };
    //Cancel
    funCancel = () => {
        this.setState({ dialogFilterVisible: false });
    };
    //sort_by_select
    sort_by_select = item => {
        this.setState({
            initial_sort_item: item.value
        });
    };
    //Duration
    duration_by_select = item => {
        this.setState({
            initial_duration_item: item.value
        });
    };
    // @action: logouts user
    funSignout = () => {
        Alert.alert("Sign out", "Signing out your account?", [
            {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
            },
            { text: "OK", onPress: this.funUserLogout }
        ]);
    };
    funUserProfile = () => {
        //const { navigate } = this.props.navigation;
        //props.navigation.navigate('UserProfile')
        alert("Profile under development");
        //navigate("UserProfile");
        //this.props.nav.navigate("UserProfile");
    };
    FunctionToOpenSecondActivity = () => { this.props.navigation.navigate('UserProfile'); }
    openDialog(show) {
        //  alert("Out side click")
        this.setState({ dialogFilterVisible: show });
    }
    funFilter = () => {
        alert("Filter under development")
        // this.setState({
        //     dialogFilterVisible: true
        // });
        // console.log("Filter");
    }
    funSearch = () => {
        alert("Search under development");
        //this.props.navigation.navigate("Search")
    };
    funModeChoose = (id) => {
        switch (id) {
            case 0:     //Direct Credit
                {
                    this.state.isDCSelected == false ? this.setState({
                        isDCSelected: true
                    }) : this.setState({
                        isDCSelected: false
                    })
                }
                break;
            case 1:     //EFT
                this.state.isEFTSelected == false ? this.setState({
                    isEFTSelected: true
                }) : this.setState({
                    isEFTSelected: false
                })
                break;
            case 2:     //SpotCash
                this.state.isSpotCashSelected == false ? this.setState({
                    isSpotCashSelected: true
                }) : this.setState({
                    isSpotCashSelected: false
                })
                break;
            case 3:     //bKash
                this.state.isbKashSelected == false ? this.setState({
                    isbKashSelected: true
                }) : this.setState({
                    isbKashSelected: false
                })
                break;
            default:
                break;

        }

    };
    componentDidMount() {
        AsyncStorage.getItem("ScreenID").then(item => {
            if (item === 1) {
                this.setState({
                    screenID: 0
                });
            }
        }); //Screen ID 1 mean it is Master
    }
    render() {
        //const { navigate } = this.props.navigation;
        const { navigation } = this.props;
        //alert(JSON.stringify(navigation))
        return (
            <View>
                <View style={{ marginTop: 20 }} />
                <View>
                    <View
                        style={{
                            flex: 0,
                            marginTop: 10,
                            marginLeft: 10,
                            marginRight: 10,
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignContent: "center",
                            alignItems: "center"
                        }}
                    >
                        <TouchableHighlight onPress={() => navigation.navigate("UserProfile")}>
                            <View
                                style={{
                                    justifyContent: "center",
                                    alignContent: "center",
                                    alignItems: "center"
                                }}
                            >
                                <Image
                                    style={{ width: 30, height: 30, borderRadius: 15 }}
                                    source={require("../assets/icons/avatar.png")}
                                />
                            </View>
                        </TouchableHighlight>
                        <View
                            style={{
                                flex: 0,
                                flexDirection: "row",
                                justifyContent: "flex-end"
                            }}
                        >
                            <TouchableOpacity onPress={() => this.funSearch()}>
                                <View>
                                    <Image
                                        style={{ width: 25, height: 25, marginRight: 10 }}
                                        source={require("../assets/icons/search.png")}
                                    />
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.funFilter()}>
                                <View>
                                    <Image
                                        style={{ width: 25, height: 25 }}
                                        source={require("../assets/icons/filter.png")}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View>
                    <Dialog
                        visible={this.state.dialogFilterVisible}
                        title="Filter Data"
                        fontFamily="Roboto-Medium"
                        contentStyle={{
                            backgroundColor: "#ffffff",
                            justifyContent: "center",
                            fontFamily: "Roboto-Medium"
                        }}
                        animationType="fade"
                        onTouchOutside={() => this.openDialog(this.state.dialogVisible)}
                    >
                        <View>
                            {/* <View
            style={{
              borderBottomColor: "#8a8c91",
              borderBottomWidth: 0.75,
              width: width - 122
            }}
          /> */}
                            <ScrollView
                                style={{
                                    height: 350,
                                    width: width - 100
                                }}
                            >
                                {/* Transaction MODE */}
                                <View>
                                    <Text
                                        style={{
                                            paddingTop: 0,
                                            paddingBottom: 10,
                                            fontSize: 13,
                                            color: "#a2a2a2",
                                            fontFamily: "Roboto-Regular"
                                        }}
                                    >
                                        {AppsString.TRANSACTION_MODE}
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        flex: 0,
                                        flexDirection: "row",
                                        justifyContent: "space-between",
                                        alignItems: "center",
                                        marginBottom: 10
                                    }}
                                >
                                    <TouchableOpacity onPress={() => this.funModeChoose(0)}>
                                        {this.state.isDCSelected === false ? <View style={[AppContainer.DetailTransactionModeView]}>
                                            <Text style={AppContainer.DetailsTransactionModeText}>
                                                {AppsString.DIRECT_CREDIT}
                                            </Text>
                                        </View> : <View style={[AppContainer.DetailTransactionModeViewSelected]}>
                                                <Text style={AppContainer.DetailsTransactionModeTextSelected}>
                                                    {AppsString.DIRECT_CREDIT}
                                                </Text>
                                            </View>}

                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.funModeChoose(1)}>
                                        {this.state.isEFTSelected === false ? <View style={[AppContainer.DetailTransactionModeView]}>
                                            <Text style={AppContainer.DetailsTransactionModeText}>
                                                {AppsString.EFT}
                                            </Text>
                                        </View> : <View style={[AppContainer.DetailTransactionModeViewSelected]}>
                                                <Text style={AppContainer.DetailsTransactionModeTextSelected}>
                                                    {AppsString.EFT}
                                                </Text>
                                            </View>}
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.funModeChoose(2)}>
                                        {this.state.isSpotCashSelected === false ? <View style={[AppContainer.DetailTransactionModeView]}>
                                            <Text style={AppContainer.DetailsTransactionModeText}>
                                                {AppsString.SOPT_CASH}
                                            </Text>
                                        </View> : <View style={[AppContainer.DetailTransactionModeViewSelected]}>
                                                <Text style={AppContainer.DetailsTransactionModeTextSelected}>
                                                    {AppsString.SOPT_CASH}
                                                </Text>
                                            </View>}
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.funModeChoose(3)}>
                                        {this.state.isbKashSelected === false ? <View style={[AppContainer.DetailTransactionModeView]}>
                                            <Text style={AppContainer.DetailsTransactionModeText}>
                                                {AppsString.BKASH}
                                            </Text>
                                        </View> : <View style={[AppContainer.DetailTransactionModeViewSelected]}>
                                                <Text style={AppContainer.DetailsTransactionModeTextSelected}>
                                                    {AppsString.BKASH}
                                                </Text>
                                            </View>}
                                    </TouchableOpacity>
                                </View>
                                {/* Sort by */}
                                <View style={{ marginTop: 10 }}>
                                    <Text
                                        style={{

                                            paddingBottom: 10,
                                            fontSize: 13,
                                            color: "#a2a2a2",
                                            fontFamily: "Roboto-Regular"
                                        }}
                                    >
                                        {AppsString.SORT_BY}
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        flex: 0,
                                        marginBottom: 15,
                                        flexDirection: "row",
                                        justifyContent: "space-between",
                                        alignItems: "center"
                                    }}
                                >/
                                    <View>
                                        <RadioForm
                                            radio_props={radio_props_sort_by}
                                            initial={this.state.initial_sort_item}
                                            formHorizontal={true}
                                            labelStyle={{
                                                fontSize: 13,
                                                color: AppColor.FILTER_RADIO_BTN_TEXT_COLOR,
                                                fontFamily: "Roboto-Regular"
                                            }}
                                            borderWidth={0.5}
                                            buttonSize={10}
                                            buttonOuterSize={20}
                                            radioStyle={{ paddingRight: 10 }}
                                            buttonColor={AppColor.FILTER_RADIO_BTN_COLOR}
                                            selectedButtonColor={AppColor.FILTER_RADIO_BTN_COLOR}
                                            onPress={item => this.sort_by_select(item)}
                                        />
                                    </View>
                                </View>
                                {/* Amount Rang */}
                                <View
                                    style={{
                                        flex: 0,
                                        flexDirection: "row",
                                        justifyContent: "space-between"
                                    }}
                                >
                                    <View>
                                        <Text
                                            style={{
                                                paddingTop: 15,
                                                paddingBottom: 0,
                                                fontFamily: "Roboto-Regular",
                                                //paddingBottom: 7,
                                                fontSize: 13,
                                                color: "#a2a2a2"
                                            }}
                                        >
                                            {AppsString.AMOUNT_RANG}
                                        </Text>
                                    </View>
                                    <View>
                                        <Text
                                            style={{
                                                paddingTop: 15,
                                                paddingBottom: 10,
                                                fontSize: 13,
                                                color: "#2ecc71",
                                                fontFamily: "Roboto-Regular"
                                            }}
                                        >
                                            {AppsString.AMOUNT}
                                        </Text>
                                    </View>
                                </View>

                                <View style={{ marginBottom: 15 }}>
                                    {/* <RangeSlider
                                        minValue={0}
                                        maxValue={100}
                                        tintColor={'#da0f22'}
                                        handleBorderWidth={1}
                                        handleBorderColor="#454d55"
                                        selectedMinimum={20}
                                        selectedMaximum={40}
                                        style={{ flex: 1, height: 70, padding: 10, backgroundColor: '#ddd' }}
                                        onChange={(data) => { console.log(data); }}
                                    /> */}
                                    <Slider
                                        minimumValue={10}
                                        maximumValue={100}
                                        step={10}
                                        value={this.state.value}
                                        thumbTintColor={AppColor.FILTER_RADIO_BTN_COLOR}
                                        onValueChange={value => this.setState({ value })}
                                        minimumTrackTintColor={AppColor.FILTER_RADIO_BTN_COLOR}
                                        maximumTrackTintColor={AppColor.SILDE_TINT_COLOR}
                                    />
                                    <Text
                                        style={{
                                            fontSize: 13,
                                            color: "#a2a2a2",
                                            fontFamily: "Roboto-Regular"
                                        }}
                                    >
                                        Value: {this.state.value}
                                    </Text>
                                </View>
                                {/* Duaration */}
                                <View style={{ marginTop: 10 }}>
                                    <Text
                                        style={{
                                            paddingBottom: 10,
                                            fontSize: 13,
                                            color: "#a2a2a2",
                                            fontFamily: "Roboto-Regular"
                                        }}
                                    >
                                        {AppsString.DURATION}
                                    </Text>
                                </View>
                                <View style={{ marginBottom: 10 }}>
                                    <RadioForm
                                        radio_props={radio_props_duration_by}
                                        initial={this.state.initial_duration_item}
                                        labelStyle={{
                                            fontSize: 13,
                                            color: AppColor.FILTER_RADIO_BTN_TEXT_COLOR,
                                            fontFamily: "Roboto-Regular"
                                        }}
                                        borderWidth={0.5}
                                        buttonSize={10}
                                        buttonOuterSize={20}
                                        radioStyle={{ paddingRight: 10 }}
                                        formHorizontal={true}
                                        buttonColor={AppColor.FILTER_RADIO_BTN_COLOR}
                                        selectedButtonColor={AppColor.FILTER_RADIO_BTN_COLOR}
                                        onPress={item => this.duration_by_select(item)}
                                        style={{ color: "red" }}
                                    />
                                </View>
                            </ScrollView>
                            <View
                                style={{
                                    flex: 0,
                                    justifyContent: 'center',
                                    flexDirection: "row"
                                }}
                            >
                                <TouchableOpacity onPress={() => this.funCancel()}>
                                    <View
                                        style={[AppContainer.DetailsHeaderContent, styles.border1]}
                                    >
                                        <Text
                                            style={[
                                                AppContainer.FilterButtonText,
                                                styles.textColorCancel
                                            ]}
                                        >
                                            {AppsString.BTN_CANCEL.toUpperCase()}
                                        </Text>
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.funApply()}>
                                    <View
                                        style={[AppContainer.DetailsHeaderContent, styles.border2]}
                                    >
                                        <Text
                                            style={[
                                                AppContainer.FilterButtonText,
                                                styles.textColorApply
                                            ]}
                                        >
                                            {AppsString.BTN_APPLY.toUpperCase()}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Dialog>
                    <View
                        style={{
                            //borderBottomColor: "#8a8c91",
                            //borderBottomWidth: 0.5,
                            width: AppsString.DEVICE_WIDTH,
                            marginTop: 5
                        }}
                    />
                    {/* <RemitTab /> */}
                </View>
            </View>
        );
    }
}
export default CommonHeader;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignSelf: "stretch",
        width: null,
        paddingTop: 0,
        backgroundColor: "#ffffff"
    },
    modeClick: {
        backgroundColor: "red"
    },
    border1: {
        borderColor: "#979797",
        borderWidth: 0.5,
        height: 50,
        width: 130,
        justifyContent: "space-around",
        //flexDirection: "row",
        alignContent: "center",
        alignItems: "center",
        margin: 5
    },
    border2: {
        borderColor: "#979797",
        borderWidth: 0.5,
        height: 50,
        backgroundColor: "#2ecc71",
        width: 130,
        justifyContent: "space-around",
        //flexDirection: "row",
        alignContent: "center",
        alignItems: "center",
        margin: 5
    },
    textColorApply: {
        color: "#ffffff"
    },
    textColorCancel: {
        color: "#1b1c1e"
    }
});
