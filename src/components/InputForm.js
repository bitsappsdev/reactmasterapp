/**
 * By       : Sree Bijoy Kumar Mohanta
 * Date     : 15 July 2018
 * Purpose  : Input Form []
 */

import React from "react";
import { KeyboardAvoidingView, StyleSheet } from "react-native";
import UserInput from "./UserInput";
import * as ConstringString from "../styles/ConstantString";
import * as AppsColor from "../styles/Color";

class InputForm extends React.Component {
  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <UserInput
          selectionColor={AppsColor.THEME_COLOR}
          placeholder={ConstringString.PLACEHOLDER_USER_NAME}
          maxLength={ConstringString.MAX_Length_USER_NAME}
          placeholderTextColor={AppsColor.PLACRHOLDER_TEXT_COLOR}
          underlineColorAndroid={AppsColor.THEME_COLOR}
          onChangeText={this.props.onChangeText}
        />
        <UserInput
          selectionColor={AppsColor.THEME_COLOR}
          placeholder={ConstringString.PLACEHOLDER_USER_PASSWORD}
          maxLength={ConstringString.MAX_Length_USER_PASSWORD}
          placeholderTextColor={AppsColor.PLACRHOLDER_TEXT_COLOR}
          underlineColorAndroid={AppsColor.THEME_COLOR}
          onChangeText={this.props.onChangeText}
        />
      </KeyboardAvoidingView>
    );
  }
}
export default InputForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  }
});
