/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Date             : 12 July 2018
 * Purpose          : Logo and Text
 */
import React from "react";
import { Image, Text, View } from "react-native";
import masterapp_logo_login from "../assets/images/masterapp_logo_login.png";
import Container from "../styles/Container";
import * as ConstantString from "../styles/ConstantString";

class Logo extends React.Component {
  render() {
    return (
      <View style={Container.LoginLogoContainer}>
        <Image source={masterapp_logo_login} style={Container.LoginLogo} />
        <Text style={Container.LoginText}>
          {ConstantString.LOGIN_TO_YOUR_ACCOUNT}
        </Text>
      </View>
    );
  }
}
export default Logo;
