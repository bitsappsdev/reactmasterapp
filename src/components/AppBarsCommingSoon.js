/**
 * Created By       : Sree Bijoy Kuamr Mohanta
 * Purpose          : Common app bars loader
 * Date             : 16/08/2018
 */
import React from 'react';
import { View, Text } from "react-native"
import { Bars } from "react-native-loader"
import * as AppColor from "../styles/Color"

class AppBarsCommingSoon extends React.Component {
    render() {
        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: "column",
                    justifyContent: "center",
                    alignContent: "center",
                    alignItems: "center"
                }}
            >
                <Text style={{ fontSize: 23, marginBottom: 10 }}>
                    Under development.
            </Text>
                {/* {<Bars size={10} color={AppColor.THEME_COLOR} />} */}
            </View>
        );
    }
}

export default AppBarsCommingSoon;