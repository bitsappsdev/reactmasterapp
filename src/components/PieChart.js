/**
 * Created By       : Md. Golam Sarwar
 * Date             : 10 Dec 2018
 * Purpose          : Component for Pie chart example
 */

import React, { Component } from 'react'
import {View } from 'react-native'
import { VictoryPie, VictoryLabel } from "victory-native";
var dateFormat = require('dateformat');

export default class PieChart extends Component {
  render() {
    return (
      <View>
      <VictoryPie
        style={{
            data: {
            fillOpacity: 0.9, stroke: "#c43a31", strokeWidth: 3
            },
            labels: {
            fontSize: 12, fill: "#c43a31"
            }
        }}
        padAngle={3}
        radius={100}
        labels={(d) => d.AMOUNT}
        labelComponent={<VictoryLabel angle={45}/>}
        labelRadius={105}
        //style={{ labels: { fill: "black", fontSize: 12, fontWeight: "bold" } }}
        data={this.props.dataList}
            x={(d) => dateFormat(d.TransactionDate, "d") + "\n" + dateFormat(d.TransactionDate, "ddd")}
            y={(d) => (d.AMOUNT)}
        />
      </View>
    )
  }
}
