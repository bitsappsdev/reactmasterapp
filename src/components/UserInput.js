/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Date             : 12 July, 2018
 * Purpose          : User Input []
 */
import React from "react";
import { TextInput, View, StyleSheet } from "react-native";
import Dimensions from "Dimensions";

class UserInput extends React.Component {
  render() {
    return (
      <View style={styles.inputWrapper}>
        <TextInput
          style={styles.input}
          selectionColor={this.props.selectionColor}
          placeholder={this.props.placeholder}
          maxLength={this.props.maxLength}
          placeholderTextColor={this.props.placeholderTextColor}
          underlineColorAndroid={this.props.underlineColorAndroid}
          onChangeText = {this.props.onChangeText}
        />
      </View>
    );
  }
}
export default UserInput;
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
const styles = StyleSheet.create({
  inputWrapper: {
    flex: 1
  },
  input: {
    width: DEVICE_WIDTH - 40,
    color: "#000"
  }
});
