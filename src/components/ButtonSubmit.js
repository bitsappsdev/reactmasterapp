/**
 * Create By    : Sree Bijoy Kumar Mohant
 * Date         : 15 Jul 2018
 * Purpose      : Submit Button
 */
import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import * as AppsString from "../styles/ConstantString";

class ButtonSubmit extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.props.onPress} style={styles.button}>
          <Text style={styles.text}>{AppsString.SUBMIT_BUTTON_TEXT}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default ButtonSubmit;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    top: 20,
    justifyContent: "flex-start"
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#02bd40",
    zIndex: 100
  },
  text: {
    fontSize: 18,
    fontFamily: "Roboto-Bold",
    color: "#fff"
  }
});
