/**
 * Created By       : Sree Bijoy Kuamr Mohanta
 * Purpose          : Common app bars loader
 * Date             : 16/08/2018
 */
import React from 'react';
import { View } from "react-native"
import { Bars } from "react-native-loader"
import * as AppColor from "../styles/Color"

class AppBars extends React.Component {
    render() {
        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "center",
                    alignContent: "center",
                    alignItems: "center"
                }}
            >
                {<Bars size={10} color={AppColor.THEME_COLOR} />}
            </View>
        );
    }
}

export default AppBars;