import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Dimensions, NetInfo } from "react-native"
const { width } = Dimensions.get('window');
function MiniOfflineSign() {
    return (
        <View style={styles.offlineContainer}>
            <Text style={styles.offlineText}>No Internet Connection</Text>
        </View>
    );
}

class OfflineNotice extends PureComponent {
    state = {
        isConnected: true
    };

    componentDidMount() {
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }

    handleConnectivityChange = isConnected => {
        if (isConnected) {
            this.setState({ isConnected });
        } else {
            this.setState({ isConnected });
        }
    };

    render() {
        console.log("IAX",this.state.isConnected)
        if (!this.state.isConnected) {
            return <MiniOfflineSign />;
        }
        return null;
    }
}


export default OfflineNotice;
const styles = StyleSheet.create({
    offlineContainer: {
        backgroundColor: '#b00020',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width,
        position: 'absolute',
        top: 25,
    },
    offlineText: {
        color: "#fff"
    }
})