/**
 * Created By       : Sree Bijoy Kumar Mohanta
 * Date             : 12 Jul 2018
 * Purpose          : Wallpaper for Login Screen
 */
import React from "react";
import { ImageBackground } from "react-native";
import bgWallPaper from "../assets/images/wallpaper.png";
import Container from "../styles/Container";
import PropTypes from "prop-types";
class Wallpaper extends React.Component {
  render() {
    return (
      <ImageBackground source={bgWallPaper} style={Container.Background}>
        {this.props.children}
      </ImageBackground>
    );
  }
}
export default Wallpaper;
