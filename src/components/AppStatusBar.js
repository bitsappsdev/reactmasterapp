/**
 * Created By   : Sree Bijoy Kumar Mohata
 * Date         : 15 Jul 2018
 * Purpose      : masterapp Status Bar
 */
import React from "react";
import { View, StatusBar, Platform, StyleSheet } from "react-native";
import * as Color from "../styles/Color";

class AppStatusBar extends React.Component {
  render() {
    return (
      <View style={Platform.OS === "ios" ? styles.statusBarIos : styles.statusBar}>
        <StatusBar
          barStyle="light-content"
          hidden={false}
          translucent backgroundColor={Color.STATUS_BAR_COLOR}
          translucent={true}
          networkActivityIndicatorVisible={true}
        // barStyle={"light-content"}
        // height={Platform.OS === 'ios' ? 20 : StatusBar.currentHeight}
        />
      </View>
    );
  }
}
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 20;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default AppStatusBar;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
    //backgroundColor: '#019b34'
  },
  statusBarIos: {
    height: STATUSBAR_HEIGHT,
    backgroundColor: '#019b34'
  },
  appBar: {
    backgroundColor: '#019b34',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#33373B',
  },
});